package personalizzazioni;

public class TempoAttesaScanner {

	private static final int MINUTI_ATTESA_GENERICO = 60 * 4;
	private static final int MINUTI_ATTESA_BISA = 60 * 1;
	
	public int getAttesaByUsername(String username) {
		final int minuti;
		minuti = username.trim().equals("5898") ? MINUTI_ATTESA_BISA : MINUTI_ATTESA_GENERICO;
		return 1000 * 60 * minuti;
	}

}
