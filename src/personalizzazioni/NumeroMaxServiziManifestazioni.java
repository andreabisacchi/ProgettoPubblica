package personalizzazioni;

public class NumeroMaxServiziManifestazioni {

	private static final int NUMMAXSERVIZI = 5;
	private static final int NUMMAXMANIFESTAZIONI = 5;
	
	private static final int NUMMAXSERVIZI_BISA = 20;
	private static final int NUMMAXMANIFESTAZIONI_BISA = 20;
	
	public int getNumeroMaxServizi(String username) {
		return username.trim().equals("5898") ? NUMMAXSERVIZI_BISA : NUMMAXSERVIZI;
	}
	
	public int getNumeroMaxManifestazioni(String username) {
		return username.trim().equals("5898") ? NUMMAXMANIFESTAZIONI_BISA : NUMMAXMANIFESTAZIONI;
	}

}
