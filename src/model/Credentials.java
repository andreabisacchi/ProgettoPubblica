package model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Semaphore;

import javax.swing.JOptionPane;

import org.identityconnectors.common.security.GuardedString;
import org.identityconnectors.common.security.GuardedString.Accessor;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

public class Credentials {
	
	private String username = null;
	private GuardedString password = null;
	
	private transient FileInputStream fileStream = null;
	
	public Credentials(String username, String password) {
		this.username = username;
		if (password == null) {
			password = null;
		} else {
			this.password = new GuardedString(password.toCharArray());
			this.password.makeReadOnly();
		}
	}
	
	public Credentials(String file) throws JsonIOException, IOException {
		this(null, null);
		
		if (!new File(file).exists()) {
			JOptionPane.showMessageDialog(null, "File " + file + " non trovato. Ne verra creato uno nuovo.", "File non trovato", JOptionPane.INFORMATION_MESSAGE);
			this.username = JOptionPane.showInputDialog("Inserisci username:");
			this.password = new GuardedString(JOptionPane.showInputDialog("Inserisci password:").toCharArray());
			this.saveCredentials(file);
		}
		
		this.constructWithFileReader(new FileInputStream(file));
		this.password.makeReadOnly();
	}
	
	/*
	 * Input file used to read the username and password
	 */
	public Credentials(FileInputStream file) throws JsonIOException {
		if (file == null)
			throw new IllegalArgumentException("File is null.");
		this.constructWithFileReader(file);
	}
	
	private void constructWithFileReader(FileInputStream file) {
		this.fileStream = file;
		this.readFromFile();
	}
	
	private void readFromFile() throws JsonIOException {
		final Gson gson = new Gson();
		JsonCredentialClass readCredentials = gson.fromJson(new InputStreamReader(fileStream), JsonCredentialClass.class);
		this.username = readCredentials.username;
		this.password = new GuardedString(readCredentials.password.toCharArray());
	}
	
	public String getUsername() {
		return username.trim();
	}
	
	public String getPassword() {
		final String[] psw = { null }; // Perche le chiusure per Java devono avere variabili final
		final Semaphore sem = new Semaphore(0);
		this.password.access(new Accessor() {
			@Override
			public void access(char[] passwdChars) {
				psw[0] = new String(passwdChars);
				sem.release();
			}
		});
		try { sem.acquire(); } catch (InterruptedException e) {	}
		return psw[0];
	}
	
	public void saveCredentials(BufferedWriter file) throws IOException {
		Gson gson = new Gson();
		file.write(gson.toJson(new JsonCredentialClass(getUsername(), getPassword())));
		file.flush();
	}
	
	public void saveCredentials(FileWriter file) throws IOException {
		this.saveCredentials(new BufferedWriter(file));
	}
	
	public void saveCredentials(String fileName) throws IOException {
		FileWriter file = new FileWriter(fileName);
		this.saveCredentials(file);
		file.close();
	}
	
	private class JsonCredentialClass {
		public String username;
		public String password;
		public JsonCredentialClass(String username, String password) {
			this.username = username;
			this.password = password;
		}
	}
	
}
