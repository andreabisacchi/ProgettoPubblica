package model;

import java.time.LocalDate;
import java.time.LocalTime;

import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioFine;
import model.interfaces.IOrarioInizio;

public class Manifestazione implements StampaCarina, IEquipaggio, IData, IOrarioFine, IOrarioInizio {

	private LocalDate data;
	private String nome;
	private String luogo;
	private String descrizione;
	private Equipaggio equipaggioPrenotati;
	private Equipaggio equipaggioConfermati;
	private LocalTime orarioInizio;
	private LocalTime orarioFine;
	private LocalTime orarioSede;

	public Manifestazione(LocalDate data, String nome, String luogo, String descrizione, Equipaggio equipaggioPrenotati,
			Equipaggio equipaggioConfermati, LocalTime orarioInizio, LocalTime orarioFine, LocalTime orarioSede) {
		super();
		this.data = data;
		this.nome = nome;
		this.luogo = luogo;
		this.descrizione = descrizione;
		this.equipaggioPrenotati = equipaggioPrenotati;
		this.equipaggioConfermati = equipaggioConfermati;
		this.orarioInizio = orarioInizio;
		this.orarioFine = orarioFine;
		this.orarioSede = orarioSede;
	}

	@Override
	public LocalDate getData() {
		return this.data;
	}

	public String getNome() {
		return nome;
	}

	public String getLuogo() {
		return luogo;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public Equipaggio getEquipaggioPrenotati() {
		return equipaggioPrenotati;
	}

	public Equipaggio getEquipaggioConfermati() {
		return equipaggioConfermati;
	}

	@Override
	public LocalTime getOrarioInizio() {
		return orarioInizio;
	}

	@Override
	public LocalTime getOrarioFine() {
		return orarioFine;
	}

	public LocalTime getOrarioSede() {
		return orarioSede;
	}

	@Override
	public String toString() {
		return data + " " + nome + " [" + luogo + "] " + descrizione + " (" + (orarioInizio != null ? orarioInizio : "<SCONOSCIUTO>") + "-" + (orarioFine != null ? orarioFine : "<SCONOSCIUTO>")
				+ ", sede: " + (orarioSede != null ? orarioSede : "<SCONOSCIUTO>") + ") --> \tPrenotati: " + (equipaggioPrenotati != null ? equipaggioPrenotati : "<NESSUNO>") + "\tConfermati: " + (equipaggioConfermati != null ? equipaggioConfermati : "<NESSUNO>");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((orarioFine == null) ? 0 : orarioFine.hashCode());
		result = prime * result + ((orarioInizio == null) ? 0 : orarioInizio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Manifestazione other = (Manifestazione) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (orarioFine == null) {
			if (other.orarioFine != null)
				return false;
		} else if (!orarioFine.equals(other.orarioFine))
			return false;
		if (orarioInizio == null) {
			if (other.orarioInizio != null)
				return false;
		} else if (!orarioInizio.equals(other.orarioInizio))
			return false;
		if (equipaggioConfermati == null) {
			if (other.equipaggioConfermati != null)
				return false;
		}
		return true;
	}

	@Override
	public String stampaCarina() {
		StringBuilder builder = new StringBuilder();

		builder.append("MANIFESTAZIONE\nData: ");
		builder.append(data.toString());
		builder.append("\nNome: ");
		builder.append(nome);
		builder.append("\nLuogo: ");
		builder.append(luogo);
		builder.append("\nDescrizione: ");
		builder.append(descrizione);
		builder.append("\nOrario: ");
		builder.append(orarioInizio.toString());
		builder.append(" - ");
		if (orarioFine != null) {
			builder.append(orarioFine.toString());
		} else {
			builder.append("??");
		}
		if (equipaggioConfermati != null) {
			builder.append("\nConfermati:\n");
			builder.append(equipaggioConfermati.stampaCarina());
		}
		if (equipaggioPrenotati != null) {
			builder.append("\nPrenotati:\n");
			builder.append(equipaggioPrenotati.stampaCarina());
		}
		builder.append("\n\n\n");

		return builder.toString();
	}

	@Override
	public Equipaggio getEquipaggio() {
		return this.getEquipaggioConfermati();
	}

}
