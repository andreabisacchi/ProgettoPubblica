package model;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JOptionPane;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class EmailList implements Collection<String> {
	
	public final Set<String> emailList;
	
	public EmailList() {
		this.emailList = new HashSet<>();
	}
	
	public EmailList(String nomeFile) throws JsonSyntaxException, JsonIOException, IOException {
		if (new File(nomeFile).exists()) {
			this.emailList = new HashSet<String>(Arrays.asList((new Gson()).fromJson(new FileReader(nomeFile), String[].class)));
		} else {
			emailList = new HashSet<>();
			emailList.add(JOptionPane.showInputDialog("Inserire un indirizzo email per la ricezione dei messaggi:"));
			String json = (new Gson()).toJson(emailList.toArray(new String[emailList.size()]));
			FileWriter file = new FileWriter(nomeFile);
			file.write(json);
			file.close();
		}
	}
	
	public EmailList(Collection<String> emails) {
		this.emailList = new HashSet<>(emails);
	}
	
	public Collection<String> getEmailList() {
		return new HashSet<>(this.emailList);
	}

	@Override
	public boolean add(String e) {
		return this.emailList.add(e);
	}

	@Override
	public boolean addAll(Collection<? extends String> c) {
		return this.emailList.addAll(c);
	}

	@Override
	public void clear() {
		this.emailList.clear();
	}

	@Override
	public boolean contains(Object o) {
		return this.emailList.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return this.emailList.containsAll(c);
	}

	@Override
	public boolean isEmpty() {
		return this.emailList.isEmpty();
	}

	@Override
	public Iterator<String> iterator() {
		return this.emailList.iterator();
	}

	@Override
	public boolean remove(Object o) {
		return this.emailList.remove(o);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return this.emailList.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return this.emailList.retainAll(c);
	}

	@Override
	public int size() {
		return this.emailList.size();
	}

	@Override
	public Object[] toArray() {
		return this.emailList.toArray();
	}

	@Override
	public <A> A[] toArray(A[] a) {
		return this.emailList.toArray(a);
	}
	
}
