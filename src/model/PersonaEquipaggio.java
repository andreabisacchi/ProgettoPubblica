package model;

public class PersonaEquipaggio implements StampaCarina {
	
	private String nome;
	private QualificaPersonaEquipaggio qualifica;
	private boolean aspirante;
	
	private boolean postoLibero = false;
	private int numeroRiga;
	
	public PersonaEquipaggio(String nome, QualificaPersonaEquipaggio qualifica, boolean aspirante) {
		super();
		this.nome = nome.trim();
		this.qualifica = qualifica;
		this.aspirante = aspirante;
		
		this.postoLibero = (nome.length() == 0);
	}
	
	public void setNumeroRiga(int numeroRiga) {
		this.numeroRiga = numeroRiga;
	}

	public String getNome() {
		return nome;
	}
	
	public QualificaPersonaEquipaggio getQualifica() {
		return qualifica;
	}
	
	public boolean isAspirante() {
		return aspirante;
	}
	
	public boolean isPostoLibero() {
		return this.postoLibero;
	}
	
	public int getNumeroRiga() {
		return this.numeroRiga;
	}

	@Override
	public String toString() {
		return (nome.length() > 0 ? nome : "<NESSUNO>") + (aspirante ? " ASPIRANTE" : "") + " in qualita di " + qualifica;
	}

	public String stampaCarina() {
		return this.nome + " " + (aspirante ? " (A)" : "");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (aspirante ? 1231 : 1237);
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((qualifica == null) ? 0 : qualifica.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonaEquipaggio other = (PersonaEquipaggio) obj;
		if (aspirante != other.aspirante)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (qualifica != other.qualifica)
			return false;
		return true;
	}
	
}
