package model.connectors;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

import model.Credentials;

public class LoginConnector {

	private final static String urlCheckLogin = "https://www.pacb.info";
	private final static String urlToConnect = "https://www.pacb.info/include/utility.php?action=login";
	private final static String urlDisconnect = "https://www.pacb.info/include/logout.php";

	public final static String ERRORURL = urlToConnect;

	private Map<String, String> cookies = new HashMap<>();

	private Credentials credenziali = null;

	public LoginConnector(Credentials credenziali) {
		this.credenziali = credenziali;
	}

	public synchronized void connect() throws IOException {
		HashMap<String, String> loginData = new HashMap<>();
		this.insertLoginData(loginData);
		Response response = StaticConnection.getConnection(urlToConnect).cookies(this.cookies).data(loginData).method(Method.POST).execute();
		this.cookies.putAll(response.cookies());
	}

	private void insertLoginData(HashMap<String, String> loginData) throws IOException {
		final String usernameName = "username";
		final String passwordName = "password";

		loginData.put(usernameName, this.credenziali.getUsername());
		loginData.put(passwordName, this.credenziali.getPassword());
	}

	public synchronized void disconnect() throws IOException {
		this.cookies = Jsoup.connect(urlDisconnect).followRedirects(true).cookies(this.cookies).execute().cookies();
	}

	public synchronized Map<String, String> getCookies() {
		return new HashMap<>(this.cookies);
	}

	public synchronized void setCookies(Map<String, String> cookies) {
		this.cookies = new HashMap<>(cookies);
	}

	public synchronized void addCookie(String name, String value) {
		this.cookies.put(name, value);
	}

	public synchronized void removeCookie(String name) {
		this.cookies.remove(name);
	}

	public synchronized boolean isConnected() throws NewsDaLeggereException {
		final Response response;
		try {
			response = StaticConnection.getConnection(urlCheckLogin).cookies(this.cookies).method(Method.GET).execute();
		} catch (IOException e) {
			this.cookies.clear();
			return false;
		}
		
		if (response == null) {
			this.cookies.clear();
			return false;
		}
		
		this.cookies.putAll(response.cookies());
		
		Connector.checkForNewsDaLeggere(response);
		
		final boolean isConnected = (response.url().toString().equals(urlCheckLogin));
		
		return isConnected;
	}
	
	public synchronized String getUsername() {
		return this.credenziali.getUsername();
	}

}
