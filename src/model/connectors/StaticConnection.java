package model.connectors;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

public class StaticConnection {
	private static final String USERAGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36";
	private static final int TIMEOUT = 10; // in seconds
	
	private static Map<String, String> header;
	
	static {
		header = new HashMap<>();
		header.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		header.put("accept-encoding", "gzip, deflate, br");
		header.put("accept-language", "it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7,en-GB;q=0.6");
		header.put("cache-control", "max-age=0");
		header.put("referer", "https://www.pacb.info/pages/servizi/servizi.php");
		header.put("upgrade-insecure-requests", "1");
	}
	
	public static Connection getConnection(String url) {
		return Jsoup.connect(url).headers(header).followRedirects(true).timeout(1000 * TIMEOUT).userAgent(USERAGENT);
	}
	
}
