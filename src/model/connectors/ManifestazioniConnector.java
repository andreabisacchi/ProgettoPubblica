package model.connectors;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import model.Equipaggio;
import model.Manifestazione;
import model.PersonaEquipaggio;
import model.QualificaPersonaEquipaggio;

public class ManifestazioniConnector extends Connector<Manifestazione> {
	
	private final static String url = "https://www.pacb.info/pages/servizi_manifestazioni/manifestazioni.php?a=manifestazioni&visu=compatta";

	public ManifestazioniConnector(LoginConnector loginConnector) {
		super(loginConnector);
	}
	
	public Set<Manifestazione> getEquipaggi(boolean prossima_data) throws IOException, NewsDaLeggereException {
		if (!this.tryConnect())
			return new HashSet<>();
		
		Set<Manifestazione> result = null;
		
		String url = ManifestazioniConnector.url;
		
		if (prossima_data)
			url += "&vado_data=after";
		else
			url += "&vado_data=now";
		
		Response response = null;
		
		response = StaticConnection.getConnection(url).cookies(this.getCookies()).method(Method.GET).execute();
		
		super.checkForNewsDaLeggere(response);
		
		this.anno = LocalDate.now().getYear(); // resetto l'anno attuale
		lastDate = null;
		result = this.parseDocument(response.parse());
		
		return result;
	}

	private Set<Manifestazione> parseDocument(Document document) {
		Set<Manifestazione> result = new HashSet<>();
		
		Elements servizi = document.select(".calendario");
		for (Element manifestazioniGiornata : servizi) {
			result.addAll(this.parseServiziGiornata(manifestazioniGiornata));
		}
		
		return result;
	}

	private Collection<? extends Manifestazione> parseServiziGiornata(Element manifestazioneGiornata) {
		Set<Manifestazione> result = new HashSet<>();
		
		String dataString = manifestazioneGiornata.selectFirst(".calendario_titolo").text().trim();
		LocalDate data = this.convertStringToDate(dataString);
		
		Elements manifestazioniGiornata = manifestazioneGiornata.select(".calendario_disponibilita");
		for (Element evento : manifestazioniGiornata) {
			String nomeEvento = evento.selectFirst(".calendario_dettagli .calendario_dettagli_titolo").text().trim();
			String luogoEvento = evento.selectFirst(".calendario_dettagli .calendario_dettagli_luogo").text().trim();
			String descrizioneEvento = evento.selectFirst(".calendario_dettagli .calendario_dettagli_descrizione").text().trim();
			String orari[] = evento.selectFirst(".calendario_dettagli .calendario_dettagli_orario").text().trim().split("-");
			LocalTime orarioInizio = null;
			LocalTime orarioFine = null;
			LocalTime orarioSede = null;
			try {
				orarioInizio = LocalTime.parse(orari[0].replaceAll("24", "00"), DateTimeFormatter.ISO_LOCAL_TIME);
			} catch (Exception e) {}
			try {
				orarioFine = LocalTime.parse(orari[1].replaceAll("24", "00"), DateTimeFormatter.ISO_LOCAL_TIME);
			} catch (Exception e) {}
			try {
				orarioSede = LocalTime.parse(evento.selectFirst(".calendario_dettagli .calendario_dettagli_orariosede").text().trim().replaceAll("24", "00").replaceAll("in sede:", "").trim(), DateTimeFormatter.ISO_LOCAL_TIME);
			} catch (Exception e) {}
			Equipaggio equipaggioPrenotato = this.parseEquipaggio(evento.selectFirst("#calendario_utenti_pren"));
			Equipaggio equipaggioConfermato = this.parseEquipaggio(evento.selectFirst("#calendario_utenti_part"));
			
			Manifestazione manifestazione = new Manifestazione(data, nomeEvento, luogoEvento, descrizioneEvento, equipaggioPrenotato, equipaggioConfermato, orarioInizio, orarioFine, orarioSede);
			result.add(manifestazione);
		}
		
		return result;
	}

	private LocalDate convertStringToDate(String dataString) {
		dataString = dataString.replaceAll("gen", "01").replaceAll("feb", "02").replaceAll("mar", "03")
		.replaceAll("apr", "04").replaceAll("mag", "05").replaceAll("giu", "06")
		.replaceAll("lug", "07").replaceAll("ago", "08").replaceAll("set", "09")
		.replaceAll("ott", "10").replaceAll("nov", "11").replaceAll("dic", "12");
		
		String[] parteDellaData = dataString.split(" ");
		
		return LocalDate.parse(parteDellaData[1] + "/" + parteDellaData[2] + "/" + this.getYear(parteDellaData), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}
	
	private int anno = LocalDate.now().getYear();
	private LocalDate lastDate = null;
	private int getYear(String[] parteDellaData) {
		final String giorno = parteDellaData[1];
		final String mese = parteDellaData[2];
		
		if (lastDate != null && Integer.parseInt(mese) < lastDate.getMonthValue()) {
			anno++; // Incremento l'anno se il mese e piu piccolo dell'ultimo visto
		}
		
		lastDate = LocalDate.parse(giorno + "/" + mese + "/" + this.anno, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		
		return anno;
	}

	private Equipaggio parseEquipaggio(Element calendarioUtenti) {
		Equipaggio equipaggio = new Equipaggio();
		
		for (Element elemento : calendarioUtenti.select(".calendario_utenti_riga")) {
			String qualifica = elemento.selectFirst("span").text().trim().toUpperCase();
			String nome = elemento.textNodes().stream().map(tn -> tn.text().trim()).collect(Collectors.joining()).trim();
			
			QualificaPersonaEquipaggio qualificaPersona = QualificaPersonaEquipaggio.getQualifica(qualifica);
			
			equipaggio.aggiungiPersonaEquipaggio(new PersonaEquipaggio(nome.replaceAll("\\(A\\)", "").trim(), qualificaPersona, nome.contains("(A)")));
			
		}
		
		if (equipaggio.getPersoneEquipaggio().size() == 0)
			return null;
		
		return equipaggio;
	}

	@Override
	public boolean prenota(Manifestazione manifestazione, QualificaPersonaEquipaggio qualifica) throws IOException {
		// TODO da fare
		return false;
	}
}
