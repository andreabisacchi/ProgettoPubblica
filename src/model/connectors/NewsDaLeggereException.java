package model.connectors;

public class NewsDaLeggereException extends Exception {

	private static final long serialVersionUID = 6771710534434382732L;

	public NewsDaLeggereException() {
	}

	public NewsDaLeggereException(String arg0) {
		super(arg0);
	}

	public NewsDaLeggereException(Throwable arg0) {
		super(arg0);
	}

	public NewsDaLeggereException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public NewsDaLeggereException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
