package model.connectors;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jsoup.Connection.Response;

import logger.Logger;
import model.QualificaPersonaEquipaggio;
import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioFine;
import model.interfaces.IOrarioInizio;

public abstract class Connector<T extends IEquipaggio & IData & IOrarioFine & IOrarioInizio> {

	private final static long AGGIORNAMENTOCACHE = 5; // In minuti
	
	private final static String urlNewsDaLeggere = "https://www.pacb.info/pages/servizi_news/news_dettaglio.php";

	private LoginConnector loginConnector;

	private Set<T> equipaggiCached = Collections.synchronizedSet(new HashSet<>());
	private LocalDateTime ultimoUpdateCache = null;

	protected abstract Set<T> getEquipaggi(boolean prossima_data) throws IOException, NewsDaLeggereException;

	public abstract boolean prenota(T t, QualificaPersonaEquipaggio qualifica) throws IOException, NewsDaLeggereException;

	public Connector(LoginConnector loginConnector) {
		this.loginConnector = loginConnector;
	}

	public final Set<T> getEquipaggi(int numeroSettimane) throws IOException, NewsDaLeggereException {
		// Inserisco questo connector nell'elenco dei connector da pulire
		synchronized (cacheCleaner.connectorsToClean) {
			cacheCleaner.connectorsToClean.add(this);
		}

		synchronized (equipaggiCached) {
			if (ultimoUpdateCache == null) { // Se non ho la cache la ricalcolo
				this.updateEquipaggi(numeroSettimane);
				this.ultimoUpdateCache = LocalDateTime.now();
			}
			return this.equipaggiCached;
		}
	}

	private final void updateEquipaggi(int numeroSettimane) throws IOException, NewsDaLeggereException {
		Set<T> temp = null;
		synchronized (equipaggiCached) {
			this.equipaggiCached.removeIf(t -> t.getData().isBefore(LocalDate.now()));
			temp = this.getEquipaggi(false);
			this.equipaggiCached.removeAll(temp);
			this.equipaggiCached.addAll(temp);
			if (numeroSettimane >= 0) {
				for (int i = 1; i < numeroSettimane; i++) {
					temp = this.getEquipaggi(true);
					this.equipaggiCached.removeAll(temp);
					this.equipaggiCached.addAll(temp);
					if (temp.size() <= 0)
						break;
				}
			}
		}
	}

	protected final Map<String, String> getCookies() {
		synchronized (this) {
			return this.loginConnector.getCookies();
		}
	}

	protected final void reconnect() throws IOException {
		synchronized (this) {
			this.loginConnector.disconnect();
			this.loginConnector.connect();
		}
	}

	protected final boolean tryConnect() throws IOException, NewsDaLeggereException {
		synchronized (this) {
			if (!this.loginConnector.isConnected())
				this.loginConnector.connect();
			
			return this.loginConnector.isConnected();
		}
	}

	public void setLoginConnector(LoginConnector loginConnector) {
		synchronized (this) {
			this.loginConnector = loginConnector;
		}
	}

	public final String getUsername() {
		synchronized (this) {
			return this.loginConnector.getUsername();
		}
	}
	
	private static final NewsDaLeggereException newsDaLeggereException = new NewsDaLeggereException("Ci sono news da leggere.");
	public static void checkForNewsDaLeggere(Response response) throws NewsDaLeggereException {
		if (response.url().toString().startsWith(urlNewsDaLeggere)) {
			throw newsDaLeggereException;
		}
	}
	
	private static class CacheCleaner extends Thread {
		public Set<Connector<?>> connectorsToClean = Collections.synchronizedSet(new HashSet<>());

		public CacheCleaner() {
			super("Connector cache cleaner");
			this.setDaemon(true);
		}

		@Override
		public void run() {
			try {
				Logger.log("Avviato cache cleaner...");
				while (!Thread.currentThread().isInterrupted()) {
					Thread.sleep(1000 * 60 * AGGIORNAMENTOCACHE);
					synchronized (connectorsToClean) {
						for (Connector<?> connector : connectorsToClean) { // Controllo ogni connector
							if (connector.ultimoUpdateCache == null || LocalDateTime.now().isAfter(connector.ultimoUpdateCache.plusMinutes(AGGIORNAMENTOCACHE))) {
								synchronized (connector.equipaggiCached) {
									connector.ultimoUpdateCache = null;
									connector.equipaggiCached.clear();
									//Logger.log("Ho pulito");
								}
							}
						}
					}
				}
			} catch (Exception e) {
				// Exit
				Logger.error("Cache Cleaner termina per eccezzione. " + e.getMessage());
			}
			Logger.log("Terminato cache cleaner");
		}
	}
	private static final CacheCleaner cacheCleaner;
	static {
		cacheCleaner = new CacheCleaner();
		cacheCleaner.start();
	}

}
