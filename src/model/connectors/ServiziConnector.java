package model.connectors;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import logger.Logger;
import model.Equipaggio;
import model.PersonaEquipaggio;
import model.QualificaPersonaEquipaggio;
import model.Servizio;

public class ServiziConnector extends Connector<Servizio>{
	
	private final static String url = "https://www.pacb.info/pages/servizi/servizi.php?a=servizi";
	private final static String urlServizi = "https://www.pacb.info/pages/servizi/servizi.php";
	private final static String urlInserimento = "https://www.pacb.info/pages/servizi/azioni.php";
	

	public ServiziConnector(LoginConnector loginConnector) {
		super(loginConnector);
	}
	
	public boolean prenota(Servizio ambulanza, QualificaPersonaEquipaggio qualifica) throws IOException, NewsDaLeggereException {
		if (ambulanza == null || ambulanza.getNumeroMacchina() == null)
			return false;
		
		String carattereQualifica =  QualificaPersonaEquipaggio.getQualificaString(qualifica);
		String numeroRiga =  this.getNumeroRigaInserimentoAmbulanza(ambulanza, qualifica);
		if (numeroRiga == null || numeroRiga.length() == 0 || carattereQualifica == null || carattereQualifica.length() == 0)
			return false;
		
		HashMap<String, String> datiInserimento = new HashMap<>();
		datiInserimento.put("inserisci_servizio", ambulanza.getNumeroMacchina());
		datiInserimento.put("equip", carattereQualifica);
		datiInserimento.put("num_riga", numeroRiga);
		datiInserimento.put("ancor", ambulanza.getNumeroServizio());
		
		final String url = ServiziConnector.urlInserimento;
		if (!this.tryConnect())
			return false;
		Response response = StaticConnection.getConnection(url).cookies(this.getCookies()).data(datiInserimento).method(Method.POST).execute();
		
		if (response.url().toString().contains(urlServizi)) {
			Optional<PersonaEquipaggio> postoAppenaOccupato = ambulanza.getEquipaggio().getPersoneEquipaggio().stream().filter(p -> p.isPostoLibero() && p.getQualifica().equals(qualifica)).findFirst();
			if (postoAppenaOccupato.isPresent()) {
				ambulanza.getEquipaggio().getPersoneEquipaggio().remove(postoAppenaOccupato.get());
				ambulanza.getEquipaggio().aggiungiPersonaEquipaggio(new PersonaEquipaggio("***AGGIUNTO***", qualifica, false));
			} else {
				Logger.error("Qui non dovrebbe arrivarci...");
			}
			return true;
		}
		
		return false;
	}
	
	private String getNumeroRigaInserimentoAmbulanza(Servizio ambulanza, QualificaPersonaEquipaggio qualifica) {
		for (PersonaEquipaggio personaEquipaggio : ambulanza.getEquipaggio()) {
			if (personaEquipaggio.isPostoLibero() && personaEquipaggio.getQualifica().equals(qualifica)) {
				return "" + personaEquipaggio.getNumeroRiga();
			}
		}
		
		return null;
	}

	public Set<Servizio> getEquipaggi(String vado_data) throws IOException, NewsDaLeggereException {
		Set<Servizio> result = null;
		
		HashMap<String, String> data = new HashMap<>();
		this.insertRequestData(data);
		
		String url = ServiziConnector.url + "&vado_data=" + vado_data;
		
		Response response = null; 
		if (!this.tryConnect())
			return new HashSet<>();		
		response = StaticConnection.getConnection(url).cookies(this.getCookies()).data(data).method(Method.POST).execute();
		
		//response = super.getResponseWithReconnect(url, Method.POST, data);
		
		this.anno = LocalDate.now().getYear(); // resetto l'anno attuale
		result = this.parseDocument(response.parse());
		
		return result;
	}
	
	public Set<Servizio> getEquipaggi(boolean prossima_data) throws IOException, NewsDaLeggereException {
		String vado_data = (prossima_data ? "after" : "now");
		
		return this.getEquipaggi(vado_data);
	}

	private Set<Servizio> parseDocument(Document document) {
		Set<Servizio> result = new HashSet<>();
		
		Elements servizi = document.select(".servizi_calendario");
		for (Element servizioGiornata : servizi) {
			result.addAll(this.parseServiziGiornata(servizioGiornata));
		}
		
		return result;
	}

	private Collection<? extends Servizio> parseServiziGiornata(Element servizioGiornata) {
		Set<Servizio> result = new HashSet<>();
		
		String dataString = servizioGiornata.selectFirst(".agenda_calendario_titolo").text().trim();
		LocalDate data = this.convertStringToDate(dataString);
		
		String numeroServizio = servizioGiornata.selectFirst("input[name='ancor']").val();
		
		Elements serviziGiornata = servizioGiornata.select(".servizi_calendario_disponibilita");
		for (Element macchina : serviziGiornata) {
			String siglaMezzo = macchina.selectFirst(".servizi_calendario_dettagli .servizi_calendario_dettagli_titolo").text().trim();
			String orari[] = macchina.selectFirst(".servizi_calendario_dettagli .servizi_calendario_dettagli_orario").text().trim().split("-");
			LocalTime orarioInizio = LocalTime.parse(orari[0].replaceAll("24", "00"), DateTimeFormatter.ISO_LOCAL_TIME);
			LocalTime orarioFine = LocalTime.parse(orari[1].replaceAll("24", "00"), DateTimeFormatter.ISO_LOCAL_TIME);
			Equipaggio equipaggio = this.parseEquipaggio(macchina.selectFirst(".servizi_calendario_utenti"));
			
			Servizio ambulanza = new Servizio(siglaMezzo, orarioInizio, orarioFine, equipaggio, data, numeroServizio);
			
			try {
				String codiceJavascript = macchina.selectFirst("a[title='inserisci servizio']").attr("onclick").toString();
				String inizioCodice = codiceJavascript.substring(codiceJavascript.indexOf('=') +2);
				ambulanza.setNumeroMacchina(inizioCodice.substring(0, inizioCodice.indexOf('\'')));
			} catch(Exception e) {}
			
			result.add(ambulanza);
		}
		
		return result;
	}

	private LocalDate convertStringToDate(String dataString) {
		dataString = dataString.replaceAll("gen", "01").replaceAll("feb", "02").replaceAll("mar", "03")
		.replaceAll("apr", "04").replaceAll("mag", "05").replaceAll("giu", "06")
		.replaceAll("lug", "07").replaceAll("ago", "08").replaceAll("set", "09")
		.replaceAll("ott", "10").replaceAll("nov", "11").replaceAll("dic", "12");
		
		String[] parteDellaData = dataString.split(" ");
		
		return LocalDate.parse(parteDellaData[1] + "/" + parteDellaData[2] + "/" + getYear(parteDellaData), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}
	
	private int anno = LocalDate.now().getYear();
	private int getYear(String[] parteDellaData) {
		final String giorno = parteDellaData[1];
		final String mese = parteDellaData[2];
		if (giorno.equals("01") && mese.equals("01")) {
			anno++; // Incremento l'anno ogni 01/01
		}
		return anno;
	}

	private Equipaggio parseEquipaggio(Element calendarioUtenti) {
		Equipaggio equipaggio = new Equipaggio();
		
		for (Element elemento : calendarioUtenti.select(".servizi_calendario_utenti_riga")) {
			String qualifica = elemento.selectFirst("span").text().trim().toUpperCase();
			String nome = elemento.textNodes().stream().map(tn -> tn.text().trim()).collect(Collectors.joining()).trim();
			
			QualificaPersonaEquipaggio qualificaPersona = QualificaPersonaEquipaggio.getQualifica(qualifica);
			
			PersonaEquipaggio personaEquipaggio = null;
			
			personaEquipaggio = new PersonaEquipaggio(nome.replaceAll("\\(A\\)", "").trim(), qualificaPersona, nome.contains("(A)"));
			
			try {
				int numeroRiga = this.recuperaNumeroRiga(elemento);
				personaEquipaggio.setNumeroRiga(numeroRiga);
			} catch (Exception e) {} ;
			
			equipaggio.aggiungiPersonaEquipaggio(personaEquipaggio);
		}
		
		return equipaggio;
	}

	private int recuperaNumeroRiga(Element elemento) {
		
		String temp = elemento.selectFirst("a[title='inserisci servizio']").attr("onclick");
		temp = temp.substring(temp.indexOf("num_riga"));
		temp = temp.substring(temp.indexOf("=") +2);
		
		temp = temp.substring(0, temp.indexOf("'"));
		return Integer.parseInt(temp);
	}

	private void insertRequestData(HashMap<String, String> data) {
		data.put("fascia_oraria", "tutti");
		data.put("servizio", "tutti");
	}

	
}
