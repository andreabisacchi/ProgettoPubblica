package model.connectors;

import java.io.IOException;
import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import controller.monitor.MonitorUtente;
import controller.monitor.UserSettings;
import logger.Logger;
import model.QualificheGestionale;

public class InformationGetterConnector {

	private final static String url = "https://www.pacb.info";
	private final static String urlGetEmail = "https://www.pacb.info/pages/profilo/contatti.php";
	
	private LoginConnector loginConnector;
	
	public InformationGetterConnector(LoginConnector loginConnector) {
		this.loginConnector = loginConnector;
	}
	
	public String getEmail() throws IOException, NewsDaLeggereException {
		final Response response = StaticConnection.getConnection(urlGetEmail).cookies(loginConnector.getCookies()).method(Method.GET).execute();
		Connector.checkForNewsDaLeggere(response);
		final Elements elements = response.parse().select("#example2>tbody>tr");
		LocalDate ultimoAggiornamento = null;
		String email = null;
		for (Element element : elements) {
			if (element.text().contains("@")) {
				LocalDate date = LocalDate.parse(element.child(0).text().trim(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				if (ultimoAggiornamento == null || date.isAfter(ultimoAggiornamento)) {
					ultimoAggiornamento = date;
					email = element.child(2).text().trim();
				}
			}
		}
		
		return email;
	}
	
	public String getNomeUtente() throws IOException, NewsDaLeggereException {
		final Response response = StaticConnection.getConnection(url).cookies(loginConnector.getCookies()).method(Method.GET).execute();
		Connector.checkForNewsDaLeggere(response);
		final Elements elements = response.parse().select(".navbar-nav>.user-menu>.dropdown-menu>li.user-header>p");
		final String toString = elements.toString(); 
		return toString.subSequence(toString.indexOf("<p>") + 3, toString.indexOf("<small>")).toString().trim();
	}
	
	public QualificheGestionale getQualifica() throws IOException, NewsDaLeggereException {
		Response response = StaticConnection.getConnection(url).cookies(loginConnector.getCookies()).method(Method.GET).execute();
		Connector.checkForNewsDaLeggere(response);
		return this.parseQualifica(response.parse());
	}
	
	public String getUrlImmagine() throws IOException, NewsDaLeggereException {
		Response response = StaticConnection.getConnection(url).cookies(loginConnector.getCookies()).method(Method.GET).execute();
		
		final Elements elements = response.parse().select("body > div > header > nav > div > ul > li.dropdown.user.user > ul > li.user-header > img");
		Connector.checkForNewsDaLeggere(response);
		final Element image = elements.first();
		
		try {
			return "https://" + new URI(url).getHost() + "/" + image.attr("src");
		} catch (Exception e) {
			return "";
		}
	}

	private QualificheGestionale parseQualifica(Document document) {
		final Elements qualificaElements = document.select(".navbar-nav>.user-menu>.dropdown-menu>li.user-header>p>small");
		final String qualifica = qualificaElements.get(0).text().trim().toLowerCase();
		if (qualifica.contains("terzo")) {
			return QualificheGestionale.TERZO;
		} else if (qualifica.contains("soccorritore") && qualifica.contains("autista")) {
			return QualificheGestionale.AUTISTASOCCORRITORE;
		} else if (qualifica.contains("soccorritore")) {
			return QualificheGestionale.SOCCORRITORE;
		} else if (qualifica.contains("autista")) {
			return QualificheGestionale.AUTISTA;
		} else {
			return null;
		}
	}
	
	
	public static void insertSettingsInMonitorUtente(MonitorUtente monitorUtente, LoginConnector loginConnector) throws IOException {
		final InformationGetterConnector info = new InformationGetterConnector(loginConnector);
		final UserSettings settings = monitorUtente.getUserSettings();
		try {
			settings.setQualifica(info.getQualifica());
			settings.setNome(info.getNomeUtente());
			settings.setUrlImmagine(info.getUrlImmagine());
			settings.clearEmails();
			settings.addEmail(info.getEmail());
			Logger.log("Ho letto le impostazioni dell'utente " + settings.getUsername() + ". Impostazioni: " + settings.toJson());
		} catch (NewsDaLeggereException e) {
			Logger.log("Errore lettura informazioni utente " + settings.getUsername() + ". Ci sono news da leggere");
		}
	}

}
