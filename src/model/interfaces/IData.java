package model.interfaces;

import java.time.LocalDate;

public interface IData {
	public LocalDate getData();
}
