package model.interfaces;

import model.Equipaggio;

public interface IEquipaggio {
	
	public Equipaggio getEquipaggio();
	
}
