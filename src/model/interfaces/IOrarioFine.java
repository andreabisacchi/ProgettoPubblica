package model.interfaces;

import java.time.LocalTime;

public interface IOrarioFine {
	LocalTime getOrarioFine();
}
