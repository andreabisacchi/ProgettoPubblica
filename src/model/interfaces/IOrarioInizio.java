package model.interfaces;

import java.time.LocalTime;

public interface IOrarioInizio {
	LocalTime getOrarioInizio();
}
