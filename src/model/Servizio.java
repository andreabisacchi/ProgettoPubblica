package model;

import java.time.LocalDate;
import java.time.LocalTime;

import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioFine;
import model.interfaces.IOrarioInizio;

public class Servizio implements StampaCarina, IEquipaggio, IData, IOrarioFine, IOrarioInizio {
	
	private String siglaMezzo;
	private LocalTime orarioInizio;
	private LocalTime orarioFine;
	private Equipaggio equipaggio;
	private LocalDate data;
	
	private String numeroMacchina = null;
	private String numeroServizio;
	
	public Servizio(String siglaMezzo, LocalTime orarioInizio, LocalTime orarioFine, Equipaggio equipaggio,
			LocalDate data, String numeroServizio) {
		super();
		this.siglaMezzo = siglaMezzo;
		this.orarioInizio = orarioInizio;
		this.orarioFine = orarioFine;
		this.equipaggio = equipaggio;
		this.data = data;
		this.numeroServizio = numeroServizio;
	}

	public String getSiglaMezzo() {
		return siglaMezzo;
	}

	@Override
	public LocalTime getOrarioInizio() {
		return orarioInizio;
	}

	@Override
	public LocalTime getOrarioFine() {
		return orarioFine;
	}

	public Equipaggio getEquipaggio() {
		return equipaggio;
	}

	@Override
	public LocalDate getData() {
		return this.data;
	}
	
	public String getNumeroMacchina() {
		return this.numeroMacchina;
	}
	
	public void setNumeroMacchina(String numeroMacchina) {
		this.numeroMacchina = numeroMacchina;
	}
	
	public String getNumeroServizio() {
		return this.numeroServizio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((orarioFine == null) ? 0 : orarioFine.hashCode());
		result = prime * result + ((orarioInizio == null) ? 0 : orarioInizio.hashCode());
		result = prime * result + ((siglaMezzo == null) ? 0 : siglaMezzo.hashCode());
		result = prime * result + ((equipaggio == null) ? 0 : equipaggio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Servizio other = (Servizio) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (orarioFine == null) {
			if (other.orarioFine != null)
				return false;
		} else if (!orarioFine.equals(other.orarioFine))
			return false;
		if (orarioInizio == null) {
			if (other.orarioInizio != null)
				return false;
		} else if (!orarioInizio.equals(other.orarioInizio))
			return false;
		if (siglaMezzo == null) {
			if (other.siglaMezzo != null)
				return false;
		} else if (!siglaMezzo.equals(other.siglaMezzo))
			return false;
		if (equipaggio == null) {
			if (other.equipaggio != null)
				return false;
		} else if (!equipaggio.equals(other.equipaggio))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return data + " " + siglaMezzo + " (" + orarioInizio + "-" + orarioFine
				+ ") --> \t" + equipaggio;
	}

	@Override
	public String stampaCarina() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("EMERGENZA\nData: ");
		builder.append(data.toString());
		builder.append("\nSigla mezzo: ");
		builder.append(siglaMezzo);
		builder.append("\nOrario: ");
		builder.append(orarioInizio.toString());
		builder.append(" - ");
		builder.append(orarioFine.toString());
		builder.append("\nEquipaggio:\n");
		builder.append(equipaggio.stampaCarina());
		builder.append("\n\n\n");
		
		return builder.toString();
	}
	
}
