package model.scanner;

import java.util.Optional;

import controller.monitor.MonitorUtente;
import controller.monitor.MonitorUtenteAttivi;
import logger.Logger;
import managers.email.EmailManager;
import model.comportamenti.Comportamento;
import model.connectors.Connector;
import model.connectors.LoginConnector;
import model.connectors.NewsDaLeggereException;
import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioFine;
import model.interfaces.IOrarioInizio;
import personalizzazioni.TempoAttesaScanner;

public class Scanner<T extends IEquipaggio & IData & IOrarioFine & IOrarioInizio> implements Runnable {

	private TempoAttesaScanner tempoAttesaScanner = new TempoAttesaScanner();
	
	private final Connector<T> connector;
	private Thread thread = null;
	private final Comportamento<T> comportamento;
	
	public Scanner(Connector<T> connector, Comportamento<T> comportamento) {
		this.connector = connector;
		this.comportamento = comportamento;
	}
	
	public void start(ThreadGroup threadGroup) {
		if (this.thread == null) {
			final Thread thread;
			if (threadGroup == null)
				thread = new Thread(this, "Thread scanner user " + this.connector.getUsername() + " (" + connector.getClass().getName() + ")");
			else 
				thread = new Thread(threadGroup, this, "Thread scanner user " + this.connector.getUsername() + " (" + connector.getClass().getName() + ")");
			thread.setDaemon(true);
			this.setThread(thread);
			this.thread.start();
		}
	}
	
	public synchronized void start() {
		this.start(null);
	}
	
	public synchronized void stop() {
		if (this.thread != null)
			this.thread.interrupt();
		this.setThread(null);	
	}
	
	private synchronized void setThread(Thread value) {
		this.thread = value;
	}
	
	@Override
	public void run() {
		boolean hoMandatoMailNewsDaLeggere = false;
		Logger.log("Lo scanner di " + this.connector.getUsername() + " sta partendo... (" + connector.getClass().getName() + ")");
		while (!Thread.currentThread().isInterrupted() && this.thread != null) {
			try {
				comportamento.invoke(this.connector);
				hoMandatoMailNewsDaLeggere = false;
			} catch (NewsDaLeggereException e) {
				if (!hoMandatoMailNewsDaLeggere) {
					Logger.log("L'utente " + this.connector.getUsername() + " ha news da leggere.");
					Optional<MonitorUtente> monitorUtente = MonitorUtenteAttivi.getInstance().get(this.connector.getUsername());
					if (monitorUtente.isPresent()) {
						try {
							EmailManager.getInstance().mandaMail("Ci sono news da leggere nel gestionale della pubblica!", "Ci sono news da leggere", monitorUtente.get().getUserSettings().getEmailList());
						} catch (Exception e1) {
							Logger.error("Errore nell'invio della mail per lettura email all'utente " + this.connector.getUsername() + ". Errore: " + e1.getMessage());
						} 
					}
				}
				hoMandatoMailNewsDaLeggere = true;
			}
			try {
				Thread.sleep(tempoAttesaScanner.getAttesaByUsername(this.connector.getUsername()));
			} catch (InterruptedException e) {
				break;
			}
			//Logger.println("Lo scanner di " + this.connector.getUsername() + " ha terminato un giro...");
		}
		Logger.log("Lo scanner termina... (" + connector.getClass().getName() + ")");
	}

	public void setLoginConnector(LoginConnector loginConnector) {
		this.connector.setLoginConnector(loginConnector);
	}
	
}
