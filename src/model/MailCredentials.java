package model;

import java.io.FileInputStream;
import java.io.IOException;

import com.google.gson.JsonIOException;

public class MailCredentials {
	
	private Credentials credenziali;
	
	public MailCredentials(String nomeFile) throws JsonIOException, IOException {
		this.credenziali = new Credentials(nomeFile);
	}
	
	public MailCredentials(FileInputStream file) throws JsonIOException {
		this.credenziali = new Credentials(file);
	}
	
	public MailCredentials(String username, String password) {
		this.credenziali = new Credentials(username, password);
	}
	
	public String getUsername() {
		return credenziali.getUsername();
	}
	
	public String getPassword() {
		return credenziali.getPassword();
	}
	
	public String getEmailAddress() {
		return credenziali.getUsername();
	}

	public String toJson() {
		return "{\"username\":\"" + this.getUsername() + "\",\"password\":\"" + this.getPassword() + "\"}";
	}
	
}
