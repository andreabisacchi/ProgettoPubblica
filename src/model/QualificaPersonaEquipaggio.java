package model;

public enum QualificaPersonaEquipaggio {
SCONOSCIUTO, AUTISTA, SOCCORRITORE, TERZO;
	
	public static QualificaPersonaEquipaggio getQualifica(String qualifica) {
		switch (qualifica) {
		case "A":
			return QualificaPersonaEquipaggio.AUTISTA;

		case "S":
			return QualificaPersonaEquipaggio.SOCCORRITORE;
			
		case "T":
		case "P":
			return QualificaPersonaEquipaggio.TERZO;
			
		default:
			return QualificaPersonaEquipaggio.SCONOSCIUTO;
		}
	}
	
	public static String getQualificaString(QualificaPersonaEquipaggio qualifica) {
		switch (qualifica) {
		case AUTISTA:
			return "A";
			
		case SOCCORRITORE:
			return "S";
			
		case TERZO:
			return "T";
		
		default:
			return "";
		}
	}
	
}
