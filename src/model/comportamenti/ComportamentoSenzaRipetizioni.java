package model.comportamenti;

import java.util.Set;

import controller.monitor.MonitorUtente;
import model.connectors.Connector;
import model.connectors.NewsDaLeggereException;
import model.filters.IFilter;
import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioFine;
import model.interfaces.IOrarioInizio;

public abstract class ComportamentoSenzaRipetizioni<T extends IEquipaggio & IData & IOrarioFine & IOrarioInizio> extends Comportamento<T> {
	
	private boolean senzaRipetizioni;
	private ComportamentoHistory<T> history = new ComportamentoHistory<>();
	
	protected ComportamentoSenzaRipetizioni(MonitorUtente monitorUtente, int numeroSettimane) {
		this(monitorUtente, numeroSettimane, false);
	}
	
	protected ComportamentoSenzaRipetizioni(MonitorUtente monitorUtente, int numeroSettimane, boolean senzaRipetizioni) {
		super(monitorUtente, numeroSettimane);
		this.senzaRipetizioni = senzaRipetizioni;
	}
	
	protected ComportamentoSenzaRipetizioni(MonitorUtente monitorUtente, int numeroSettimane, boolean senzaRipetizioni, IFilter<T> filtro) {
		super(monitorUtente, numeroSettimane, filtro);
		this.senzaRipetizioni = senzaRipetizioni;
	}
	
	@Override
	public void invoke(Connector<T> connector) throws NewsDaLeggereException {
		Set<T> insieme = this.getSet(connector);
		
		if (this.senzaRipetizioni) {
			insieme = this.history.rimuoviGiaProcessati(insieme);
		}
		
		if (insieme.size() > 0) {
			this.invoke(insieme);
		}
	}
	
	public boolean stessaHistory(ComportamentoSenzaRipetizioni<T> comportamento) {
		return (this.history.equals(comportamento.history));
	}
	
	public void clearHistory() {
		this.history.clear();
	}
	
	protected void remove(T t) {
		this.history.consideraComeNonVisto(t);
	}
	
}
