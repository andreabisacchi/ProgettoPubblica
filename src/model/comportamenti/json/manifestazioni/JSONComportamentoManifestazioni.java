package model.comportamenti.json.manifestazioni;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import controller.monitor.MonitorUtente;
import model.Manifestazione;
import model.comportamenti.Comportamento;
import model.comportamenti.ComportamentoMandaMail;
import model.comportamenti.ListaComportamenti;
import model.filters.FiltroDescrizioneManifestazione;
import model.filters.FiltroLuogoManifestazione;
import model.filters.IFilter;

public class JSONComportamentoManifestazioni {
	public String nome;
	public JSONFiltroManifestazioni filtro;
	
	public Optional<Comportamento<Manifestazione>> getComportamento(MonitorUtente monitor) {
		final Optional<Comportamento<Manifestazione>> result;
		
		if (this.nome.equals(ComportamentoMandaMail.NOMEAZIONE)) {
			final Comportamento<Manifestazione> comportamento = new ComportamentoMandaMail<Manifestazione>(monitor, 4);
			result = Optional.of(comportamento);
			IFilter<Manifestazione> nuovoFiltro = null;

			if (this.filtro.nome.equals(FiltroLuogoManifestazione.NOMEFILTRO)) {
				nuovoFiltro = new FiltroLuogoManifestazione(this.filtro.data.luogo);
			} else if (this.filtro.nome.equals(FiltroDescrizioneManifestazione.NOMEFILTRO)) {
				nuovoFiltro = new FiltroDescrizioneManifestazione(this.filtro.data.descrizione);
			}

			comportamento.setFiltro(nuovoFiltro);
		} else {
			result = Optional.empty();
		}
		
		return result;
	}
	
	public static JSONComportamentoManifestazioni fromComportamento(Comportamento<Manifestazione> comportamento) {
		JSONComportamentoManifestazioni result = new JSONComportamentoManifestazioni();
		result.nome = comportamento.getNome();
		final IFilter<Manifestazione> filtro = comportamento.getFiltro();
		if (filtro != null) {
			result.filtro = new JSONFiltroManifestazioni();
			if (filtro instanceof FiltroLuogoManifestazione) {
				result.filtro.nome = FiltroLuogoManifestazione.NOMEFILTRO;
				result.filtro.data = new JSONDataManifestazioni();
				result.filtro.data.luogo = ((FiltroLuogoManifestazione) filtro).getLuogo();
			} else if (filtro instanceof FiltroDescrizioneManifestazione) {
				result.filtro.nome = FiltroDescrizioneManifestazione.NOMEFILTRO;
				result.filtro.data = new JSONDataManifestazioni();
				result.filtro.data.descrizione = ((FiltroDescrizioneManifestazione) filtro).getDescrizione();
			}
		} else {
			result.filtro = null;
		}
		return result;
	}
	
	private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	public static String jsonFromComportamento(Comportamento<Manifestazione> comportamento) {
		final LinkedList<JSONComportamentoManifestazioni> listToJSON = getAllComportamenti(comportamento);
		return gson.toJson(listToJSON);
	}
	
	private static LinkedList<JSONComportamentoManifestazioni> getAllComportamenti(Comportamento<Manifestazione> comportamento) {
		final LinkedList<JSONComportamentoManifestazioni> result = new LinkedList<>();
		if (comportamento instanceof ListaComportamenti) {
			((ListaComportamenti<Manifestazione>) comportamento).forEach(c -> result.addAll(getAllComportamenti(c)));
		} else {
			result.add(JSONComportamentoManifestazioni.fromComportamento(comportamento));
		}
		return result;
	}
	
	public static Collection<Comportamento<Manifestazione>> getComportamentiFromFile(File file, MonitorUtente monitor) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		final HashSet<Comportamento<Manifestazione>> result = new HashSet<>();
		final JSONComportamentoManifestazioni[] arrayRead = gson.fromJson(new JsonReader(new FileReader(file)), JSONComportamentoManifestazioni[].class);
		for (JSONComportamentoManifestazioni jsonComportamentoManifestazioni : arrayRead) {
			Optional<Comportamento<Manifestazione>> optional = jsonComportamentoManifestazioni.getComportamento(monitor);
			if (optional.isPresent())
				result.add(optional.get());
		}
		return result;
	}
	
}
