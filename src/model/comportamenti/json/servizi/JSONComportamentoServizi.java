package model.comportamenti.json.servizi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import controller.monitor.MonitorUtente;
import model.QualificheGestionale;
import model.Servizio;
import model.comportamenti.Comportamento;
import model.comportamenti.ComportamentoMandaMail;
import model.comportamenti.ListaComportamenti;
import model.filters.FasciaOraria;
import model.filters.FiltroIoSegnato;
import model.filters.FiltroOrarioGiornoSettimana;
import model.filters.FiltroPromemoria;
import model.filters.IFilter;

public class JSONComportamentoServizi {
	public String nome;
	public JSONFiltroServizi filtro;
	
	public Optional<Comportamento<Servizio>> getComportamento(MonitorUtente monitor) {
		final Optional<Comportamento<Servizio>> result;
		
		if (this.nome.equals(ComportamentoMandaMail.NOMEAZIONE)) {
			final Comportamento<Servizio> comportamento = new ComportamentoMandaMail<Servizio>(monitor, 4);
			result = Optional.of(comportamento);
			IFilter<Servizio> nuovoFiltro = null;

			if (this.filtro.nome.equals(FiltroIoSegnato.NOMEFILTRO)) {
				nuovoFiltro = new FiltroIoSegnato<>(monitor);
			} else if (this.filtro.nome.equals(FiltroPromemoria.NOMEFILTRO)) {
				nuovoFiltro = new FiltroPromemoria<>(monitor);
			} else if (this.filtro.nome.equals(FiltroOrarioGiornoSettimana.NOMEFILTRO)) {
				Collection<FasciaOraria> fasceOrarie = Arrays.asList(this.filtro.data.fasciaOraria);
				Collection<DayOfWeek> giorniDellaSettimana = Arrays.asList(this.filtro.data.giorniSettimana);
				QualificheGestionale qualifica = this.filtro.data.qualifica;
				nuovoFiltro = new FiltroOrarioGiornoSettimana(fasceOrarie, giorniDellaSettimana, qualifica);
			}

			comportamento.setFiltro(nuovoFiltro);
		} else {
			result = Optional.empty();
		}
		
		return result;
	}
	
	public static JSONComportamentoServizi fromComportamento(Comportamento<Servizio> comportamento) {
		JSONComportamentoServizi result = new JSONComportamentoServizi();
		result.nome = comportamento.getNome();
		final IFilter<Servizio> filtro = comportamento.getFiltro();
		if (filtro != null) {
			result.filtro = new JSONFiltroServizi();
			if (filtro instanceof FiltroIoSegnato) {
				result.filtro.nome = FiltroIoSegnato.NOMEFILTRO;
				result.filtro.data = null;
			} else if (filtro instanceof FiltroPromemoria) {
				result.filtro.nome = FiltroPromemoria.NOMEFILTRO;
				result.filtro.data = null;
			} else if (filtro instanceof FiltroOrarioGiornoSettimana) {
				result.filtro.nome = FiltroOrarioGiornoSettimana.NOMEFILTRO;
				result.filtro.data = new JSONDataServizi();
				FiltroOrarioGiornoSettimana filtroOrarioGiornoSettimana = (FiltroOrarioGiornoSettimana) filtro;
				Set<FasciaOraria> fasceOrarie = filtroOrarioGiornoSettimana.getFasceOrarie();
				result.filtro.data.fasciaOraria = fasceOrarie.toArray(new FasciaOraria[fasceOrarie.size()]);
				Set<DayOfWeek> giornisettimana = filtroOrarioGiornoSettimana.getGiorniDellaSettimana();
				result.filtro.data.giorniSettimana = giornisettimana.toArray(new DayOfWeek[giornisettimana.size()]);
				result.filtro.data.qualifica = filtroOrarioGiornoSettimana.getQualifica();
			}
		} else {
			result.filtro = null;
		}
		return result;
	}
	
	private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	public static String jsonFromComportamento(Comportamento<Servizio> comportamento) {
		final LinkedList<JSONComportamentoServizi> listToJSON = getAllComportamenti(comportamento);
		return gson.toJson(listToJSON);
	}
	
	private static LinkedList<JSONComportamentoServizi> getAllComportamenti(Comportamento<Servizio> comportamento) {
		final LinkedList<JSONComportamentoServizi> result = new LinkedList<>();
		if (comportamento instanceof ListaComportamenti) {
			((ListaComportamenti<Servizio>) comportamento).forEach(c -> result.addAll(getAllComportamenti(c)));
		} else {
			result.add(JSONComportamentoServizi.fromComportamento(comportamento));
		}
		return result;
	}
	
	public static Collection<Comportamento<Servizio>> getComportamentiFromFile(File file, MonitorUtente monitor) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		final HashSet<Comportamento<Servizio>> result = new HashSet<>();
		final JSONComportamentoServizi[] arrayRead = gson.fromJson(new JsonReader(new FileReader(file)), JSONComportamentoServizi[].class);
		for (JSONComportamentoServizi jsonComportamentoServizi : arrayRead) {
			Optional<Comportamento<Servizio>> optional = jsonComportamentoServizi.getComportamento(monitor);
			if (optional.isPresent())
				result.add(optional.get());
		}
		return result;
	}
	
}
