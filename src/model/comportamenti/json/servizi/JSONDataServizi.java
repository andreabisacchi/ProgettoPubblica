package model.comportamenti.json.servizi;

import java.time.DayOfWeek;

import model.QualificheGestionale;
import model.filters.FasciaOraria;

public class JSONDataServizi {
	public FasciaOraria[] fasciaOraria;
	public DayOfWeek[] giorniSettimana;
	public QualificheGestionale qualifica;
}
