package model.comportamenti;

import java.util.Set;
import java.util.stream.Stream;

import controller.monitor.MonitorUtente;
import model.QualificaPersonaEquipaggio;
import model.connectors.Connector;
import model.connectors.NewsDaLeggereException;
import model.filters.IFilter;
import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioFine;
import model.interfaces.IOrarioInizio;

public class ComportamentoPrenotazione<T extends IEquipaggio & IData & IOrarioFine & IOrarioInizio> extends Comportamento<T> {
	
	private static final String NOMEAZIONE = "prenota";
	
	private final QualificaPersonaEquipaggio qualifica;
	
	public ComportamentoPrenotazione(MonitorUtente monitorUtente, int numeroSettimane, QualificaPersonaEquipaggio qualifica) {
		this(monitorUtente, numeroSettimane, qualifica, null);
	}
	
	 public ComportamentoPrenotazione(MonitorUtente monitorUtente, int numeroSettimane, QualificaPersonaEquipaggio qualifica, IFilter<T> filtro) {
		super(monitorUtente, numeroSettimane, filtro);
		this.qualifica = qualifica;
	}
	
	@Override
	public void invoke(Connector<T> connector) throws NewsDaLeggereException {
		Stream<T> stream = super.getSet(connector).stream().parallel();
		
		if (this.getFiltro() != null)
			stream = stream.filter(t -> this.getFiltroPerComportamento().filter(t));
		
		stream.forEach(a -> {
			try {
				connector.prenota(a, this.qualifica);
			} catch (Exception e) {}
		});
	}
	
	@Override
	public void invoke(Set<T> insieme) {
		// Doesn't have a specific behavior for a set
	}

	@Override
	public String toJson() {
		return "{\"nome\":\"" + NOMEAZIONE + "\",\"ID\":\"" + this.getID() + "\",\"filtro\":" + this.getFiltro().toJson() + "},\"data\":" + this.getDataValue() + "";
	}

	private String getDataValue() {
		return "null";
	}
	
	public static String getNomeAzione() {
		return NOMEAZIONE;
	}

	@Override
	public String getNome() {
		return NOMEAZIONE;
	}
	
}
