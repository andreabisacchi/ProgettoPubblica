package model.comportamenti;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.HashSet;
import java.util.Set;

import model.interfaces.IData;
import model.interfaces.IEquipaggio;

public class ComportamentoHistory<T extends IEquipaggio & IData> {
	private final Set<T> giaVisti = new HashSet<>();
	
	public void clear() {
		this.giaVisti.clear();
	}
	
	public Set<T> rimuoviGiaProcessati(Set<T> insieme) {
		final LocalDate domenicaPrecedente = LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));
		this.giaVisti.removeIf(t -> t.getData().isBefore(domenicaPrecedente));
		insieme.removeAll(this.giaVisti);
		this.giaVisti.addAll(insieme);
		return insieme;
	}
	
	public void consideraComeNonVisto(T t) {
		this.giaVisti.remove(t);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((giaVisti == null) ? 0 : giaVisti.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComportamentoHistory<?> other = (ComportamentoHistory<?>) obj;
		if (giaVisti == null) {
			if (other.giaVisti != null)
				return false;
		} else if (!giaVisti.equals(other.giaVisti))
			return false;
		return true;
	}
	
}
