package model.comportamenti;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import controller.monitor.MonitorUtente;
import model.connectors.Connector;
import model.connectors.NewsDaLeggereException;
import model.filters.FilterList;
import model.filters.FiltroNonNelPassato;
import model.filters.IFilter;
import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioFine;
import model.interfaces.IOrarioInizio;

public abstract class Comportamento<T extends IEquipaggio & IData & IOrarioFine & IOrarioInizio> {

	private int numeroSettimane;
	private int ID = -1;

	private Set<T> insieme = new HashSet<>();

	private final MonitorUtente monitorUtente;

	private IFilter<T> filtro;

	protected Comportamento(MonitorUtente monitorUtente) {
		this(monitorUtente, 1, null);
	}

	protected Comportamento(MonitorUtente monitorUtente, int numeroSettimane) {
		this(monitorUtente, numeroSettimane, null);
	}

	protected Comportamento(MonitorUtente monitorUtente, int numeroSettimane, IFilter<T> filtro) {
		if (numeroSettimane < 0)
			throw new IllegalArgumentException("numeroSettimane negativo!");

		this.numeroSettimane = numeroSettimane;
		this.monitorUtente = monitorUtente;

		this.setFiltro(filtro);
	}

	public IFilter<T> getFiltro() {
		return this.filtro;
	}

	public void setFiltro(IFilter<T> filtro) {
		this.filtro = filtro;
	}

	private final FiltroNonNelPassato<T> filtroNonNelPassato = new FiltroNonNelPassato<>();
	private final FilterList<T> filterList = new FilterList<>();
	protected IFilter<T> getFiltroPerComportamento() {
		filterList.clear();
		filterList.add(this.getFiltro());
		filterList.add(filtroNonNelPassato);
		return filterList;
	}

	protected final Set<T> getSet(Connector<T> connector) throws NewsDaLeggereException {
		try {
			insieme.addAll(connector.getEquipaggi(numeroSettimane));
		} catch (IOException e) {}
		return insieme;
	}

	protected MonitorUtente getMonitorUtente() {
		return this.monitorUtente;
	}

	public void invoke(Connector<T> connector) throws NewsDaLeggereException {
		this.invoke(this.getSet(connector));
	}

	public abstract void invoke(Set<T> insieme) throws NewsDaLeggereException;

	public final int getID() {
		return this.ID;
	}

	public final void setID(int id) {
		this.ID = id;
	}

	public abstract String toJson();
	
	public abstract String getNome();

}
