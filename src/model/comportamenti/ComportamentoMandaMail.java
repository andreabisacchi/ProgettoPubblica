package model.comportamenti;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.mail.MessagingException;

import controller.monitor.MonitorUtente;
import managers.email.EmailManager;
import model.StampaCarina;
import model.filters.IFilter;
import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioFine;
import model.interfaces.IOrarioInizio;

public class ComportamentoMandaMail<T extends StampaCarina & IEquipaggio & IData & IOrarioFine & IOrarioInizio> extends ComportamentoSenzaRipetizioni<T> {

	public static final String NOMEAZIONE = "email";

	private ExecutorService executor;

	public ComportamentoMandaMail(MonitorUtente monitorUtente, int numeroSettimane) {
		this(monitorUtente, numeroSettimane, null);
	}

	public ComportamentoMandaMail(MonitorUtente monitorUtente, int numeroSettimane, IFilter<T> filtro) {
		super(monitorUtente, numeroSettimane, true, filtro);
	}

	@Override
	public void invoke(Set<T> insieme) {
		synchronized (insieme) {
			executor = Executors.newSingleThreadExecutor();
			executor.execute(() -> {
				Stream<T> stream = insieme.stream();

				if (this.getFiltro() != null)
					stream = stream.filter(t -> this.getFiltroPerComportamento().filter(t));

				final Set<T> daMandare = stream.collect(Collectors.toSet());

				daMandare.forEach(t -> {
					try {
						EmailManager.getInstance().mandaMail(t, this.getMonitorUtente().getUserSettings().getEmailList());
					} catch (MessagingException e) {
						System.err.println("Errore invio mail.");
						this.remove(t);
					}
				});
			});
			executor.shutdown();
		}
	}

	@Override
	public String toJson() {
		return "{\"nome\":\"" + NOMEAZIONE + "\",\"ID\":\"" + this.getID() + "\",\"filtro\":" + (this.getFiltro() != null ? this.getFiltro().toJson() : "undefined" ) + ",\"data\":" + this.getDataValue() + "}";
	}

	private String getDataValue() {
		return "{}";
	}

	@Override
	public String getNome() {
		return NOMEAZIONE;
	}

}

