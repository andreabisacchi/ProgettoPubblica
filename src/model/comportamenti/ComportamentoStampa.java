package model.comportamenti;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import controller.monitor.MonitorUtente;
import model.StampaCarina;
import model.filters.IFilter;
import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioFine;
import model.interfaces.IOrarioInizio;

public class ComportamentoStampa<T extends StampaCarina & IEquipaggio & IData & IOrarioFine & IOrarioInizio> extends ComportamentoSenzaRipetizioni<T> {

	private static final String NOMEAZIONE = "stampa";
	
	public ComportamentoStampa(MonitorUtente monitorUtente, int numeroSettimane) {
		this(monitorUtente, numeroSettimane, null);
	}
	
	public ComportamentoStampa(MonitorUtente monitorUtente, int numeroSettimane, IFilter<T> filtro) {
		this(monitorUtente, numeroSettimane, filtro, false);
	}
	
	public ComportamentoStampa(MonitorUtente monitorUtente, int numeroSettimane, IFilter<T> filtro, boolean mantieniStampa) {
		super(monitorUtente, numeroSettimane, mantieniStampa, filtro);
	}
	
	@Override
	public void invoke(Set<T> insieme) {
		Stream<T> stream = insieme.stream().parallel();
		
		if (this.getFiltro() != null)
			stream = stream.filter(t -> this.getFiltroPerComportamento().filter(t));
		
		final Set<T> insiemeDaStampare = stream.collect(Collectors.toSet());
		
		insiemeDaStampare.forEach(t -> System.out.println(t.stampaCarina()));
	}

	@Override
	public String toJson() {
		return "{\"nome\":\"" + NOMEAZIONE + "\",\"ID\":\"" + this.getID() + "\",\"filtro\":" + (this.getFiltro() != null ? this.getFiltro().toJson() : "null") + ",\"data\":" + this.getDataValue() + "}";
	}

	private String getDataValue() {
		return "null";
	}
	
	public static String getNomeAzione() {
		return NOMEAZIONE;
	}

	@Override
	public String getNome() {
		return NOMEAZIONE;
	}
	
}
