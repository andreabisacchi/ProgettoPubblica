package model.comportamenti;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedDeque;

import controller.monitor.MonitorUtente;
import model.connectors.Connector;
import model.connectors.NewsDaLeggereException;
import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioFine;
import model.interfaces.IOrarioInizio;

public class ListaComportamenti<T extends IEquipaggio & IData & IOrarioFine & IOrarioInizio> extends Comportamento<T> implements Collection<Comportamento<T>> {

	private static final String NOMEAZIONE = "lista";

	private final ConcurrentLinkedDeque<Comportamento<T>> comportamenti = new ConcurrentLinkedDeque<>();

	public ListaComportamenti(MonitorUtente monitorUtente) {
		super(monitorUtente);
	}	
	
	@Override
	public synchronized void invoke(Connector<T> connector) throws NewsDaLeggereException {
		for (Comportamento<T> comportamento : comportamenti) {
			comportamento.invoke(connector);
		}
	}

	@Override
	public synchronized void invoke(Set<T> insieme) {
		// Doens't have a specific behavior
	}

	/***** COLLECTION METHODS *****/

	@Override
	public synchronized boolean add(Comportamento<T> e) {
		return this.comportamenti.add(e);
	}

	@Override
	public synchronized boolean addAll(Collection<? extends Comportamento<T>> c) {
		return this.comportamenti.addAll(c);
	}

	@Override
	public synchronized void clear() {
		this.comportamenti.clear();
	}

	@Override
	public synchronized boolean contains(Object o) {
		return this.comportamenti.contains(o);
	}

	@Override
	public synchronized boolean containsAll(Collection<?> c) {
		return this.comportamenti.containsAll(c);
	}

	@Override
	public synchronized boolean isEmpty() {
		return this.comportamenti.isEmpty();
	}

	@Override
	public synchronized Iterator<Comportamento<T>> iterator() {
		return this.comportamenti.iterator();
	}

	@Override
	public synchronized boolean remove(Object o) {
		return this.comportamenti.remove(o);
	}

	@Override
	public synchronized boolean removeAll(Collection<?> c) {
		return this.comportamenti.removeAll(c);
	}

	@Override
	public synchronized boolean retainAll(Collection<?> c) {
		return this.comportamenti.retainAll(c);
	}

	@Override
	public synchronized int size() {
		return this.comportamenti.size();
	}

	@Override
	public synchronized Object[] toArray() {
		return this.comportamenti.toArray();
	}

	@Override
	public synchronized <A> A[] toArray(A[] a) {
		return this.comportamenti.toArray(a);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String toJson() {
		final Comportamento[] array = this.comportamenti.toArray(new Comportamento[this.comportamenti.size()]);
		
		if (array.length == 1)
			return array[0].toJson();
		
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEAZIONE + "\",\"ID\":\"" + this.getID() + "\",\"azioni\":[");

		if (array.length > 0) {
			for (Comportamento comportamento : array) {
				result.append(comportamento.toJson());
				result.append(',');
			}
			result.setCharAt(result.lastIndexOf(","), ']');
		} else {
			result.append(']');
		}

		result.append(",\"data\":" + this.getDataValue() + "}");
		return result.toString();
	}
	
	private String getDataValue() {
		return "null";
	}
	
	public static String getNomeAzione() {
		return NOMEAZIONE;
	}

	@Override
	public String getNome() {
		return NOMEAZIONE;
	}

}
