package model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Equipaggio implements Iterable<PersonaEquipaggio>, StampaCarina {

	private Set<PersonaEquipaggio> persone = new HashSet<>();

	public void aggiungiPersonaEquipaggio(PersonaEquipaggio persona) {
		this.persone.add(persona);
	}

	public Set<PersonaEquipaggio> getPersoneEquipaggio() {
		return this.persone;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		StringBuilder builderDefault = new StringBuilder();
		StringBuilder builderAutisti = new StringBuilder();
		StringBuilder builderSoccorritori = new StringBuilder();
		StringBuilder builderTerzi = new StringBuilder();

		for (PersonaEquipaggio personaEquipaggio : persone) {
			switch (personaEquipaggio.getQualifica()) {
			case AUTISTA:
				builderAutisti.append(personaEquipaggio.toString() + " ");
				break;
			case SOCCORRITORE:
				builderSoccorritori.append(personaEquipaggio.toString() + " ");
				break;
			case TERZO:
				builderTerzi.append(personaEquipaggio.toString() + " ");
				break;

			default:
				builderDefault.append(personaEquipaggio.toString() + " ");
				break;
			}
		}

		builder.append(builderAutisti).append(builderSoccorritori).append(builderTerzi).append(builderDefault);

		return builder.toString();
	}

	@Override
	public Iterator<PersonaEquipaggio> iterator() {
		return this.persone.iterator();
	}

	@Override
	public String stampaCarina() {
		StringBuilder builder = new StringBuilder();

		HashMap<QualificaPersonaEquipaggio, StringBuilder> mappaStringBuilder = new HashMap<>();
		for (PersonaEquipaggio personaEquipaggio : persone) {
			if (personaEquipaggio.getNome().length() > 0) {
				StringBuilder builderPersona = mappaStringBuilder.get(personaEquipaggio.getQualifica());
				if (builderPersona == null)
					builderPersona = new StringBuilder();
				builderPersona.append("\t");
				builderPersona.append(personaEquipaggio.stampaCarina());
				builderPersona.append("\n");
				mappaStringBuilder.put(personaEquipaggio.getQualifica(), builderPersona);
			}
		}

		QualificaPersonaEquipaggio[] priorita = {QualificaPersonaEquipaggio.AUTISTA, QualificaPersonaEquipaggio.SOCCORRITORE, QualificaPersonaEquipaggio.TERZO};
		for (QualificaPersonaEquipaggio qualifica : priorita) {
			StringBuilder builderQualifica = mappaStringBuilder.get(qualifica);
			if (builderQualifica != null) {
				builder.append(qualifica.toString());
				builder.append("\n");
				builder.append(builderQualifica.toString());
			}
		}

		return builder.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((persone == null) ? 0 : persone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Equipaggio other = (Equipaggio) obj;
		if (persone == null) {
			if (other.persone != null)
				return false;
		} else if (!persone.equals(other.persone))
			return false;
		return true;
	}

}
