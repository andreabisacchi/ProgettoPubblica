package model.filters;

import model.Manifestazione;

public class FiltroLuogoManifestazione implements IFilter<Manifestazione> {

	public static final String NOMEFILTRO = "luogoManifestazione";

	private final String luogo;
	
	public FiltroLuogoManifestazione(String luogo) {
		this.luogo = luogo.toLowerCase().trim();
	}

	@Override
	public boolean filter(Manifestazione manifestazione) {
		return (manifestazione.getLuogo().toLowerCase().contains(this.luogo));
	}

	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[],\"data\":" + this.getDataValue() + "}");
		return result.toString();
	}
	
	private String getDataValue() {
		return "{\"luogo\":\"" + this.luogo + "\"}";
	}

	public String getLuogo() {
		return this.luogo;
	}

}
