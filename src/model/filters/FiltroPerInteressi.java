package model.filters;

import java.util.HashSet;
import java.util.Set;

import com.google.gson.Gson;

public abstract class FiltroPerInteressi<T> implements IFilter<T> {
	private static final String NOMEFILTRO = "manifestazioniCheInteressano";
	
	private Set<String> interessi = new HashSet<>();
	
	public void addInteresse(String iteresse) {
		synchronized (interessi) {
			this.interessi.add(iteresse);
		}
	}
	
	public void removeInteresse(String interesse) {
		synchronized (interessi) {
			this.interessi.remove(interesse);
		}
	}
	
	@Override
	public final boolean filter(T t) {
		synchronized (interessi) {
			String nome = getNome(t).toLowerCase();
			for (String interesse : interessi) {
				if (nome.contains(interesse.toLowerCase()))
					return true;
			}
			return false;
		}
	}
	
	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[],\"data\":" + this.getDataValue() + "}");
		return result.toString();
	}
	
	private String getDataValue() {
		return "{\"interessi\":" + new Gson().toJson(this.interessi.toArray(new String[this.interessi.size()])) + "}";
	}
	
	public static String getNomeFiltro() {
		return NOMEFILTRO;
	}
	
	protected abstract String getNome(T t);
	
}
