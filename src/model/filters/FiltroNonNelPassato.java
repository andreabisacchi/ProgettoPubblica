package model.filters;

import java.time.LocalDateTime;

import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioInizio;

public class FiltroNonNelPassato<T extends IEquipaggio & IData & IOrarioInizio> implements IFilter<T> {

	private static final String NOMEFILTRO = "filtroNonNelPassato";

	@Override
	public boolean filter(T t) {
		final LocalDateTime now = LocalDateTime.now();
		final LocalDateTime tDate = LocalDateTime.of(t.getData(), t.getOrarioInizio());
		return tDate.isAfter(now) || tDate.isEqual(now);
	}

	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[],\"data\":{}}");
		return result.toString();
	}


}
