package model.filters;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedDeque;

public class FilterList<T> implements IFilter<T>, Collection<IFilter<T>> {

	private static final String NOMEFILTRO = "listaFiltri";

	private final ConcurrentLinkedDeque<IFilter<T>> lista = new ConcurrentLinkedDeque<>();

	public Collection<IFilter<T>> getListaFiltri() {
		return this.lista;
	}

	@Override
	public boolean filter(T t) {
		boolean result = true;
		synchronized (lista) {
			for (IFilter<T> filter : lista) {
				result = result && filter.filter(t);
			}
		}
		return result;
	}

	@Override
	public synchronized boolean add(IFilter<T> e) {
		return this.lista.add(e);
	}

	@Override
	public synchronized boolean addAll(Collection<? extends IFilter<T>> c) {
		return this.lista.addAll(c);
	}

	@Override
	public synchronized void clear() {
		this.lista.clear();
	}

	@Override
	public synchronized boolean contains(Object o) {
		return this.lista.contains(o);
	}

	@Override
	public synchronized boolean containsAll(Collection<?> c) {
		return this.lista.containsAll(c);
	}

	@Override
	public synchronized boolean isEmpty() {
		return this.lista.isEmpty();
	}

	@Override
	public synchronized Iterator<IFilter<T>> iterator() {
		return this.lista.iterator();
	}

	@Override
	public synchronized boolean remove(Object o) {
		return this.lista.remove(o);
	}

	@Override
	public synchronized boolean removeAll(Collection<?> c) {
		return this.removeAll(c);
	}

	@Override
	public synchronized boolean retainAll(Collection<?> c) {
		return this.lista.retainAll(c);
	}

	@Override
	public synchronized int size() {
		return this.lista.size();
	}

	@Override
	public synchronized Object[] toArray() {
		return this.lista.toArray();
	}

	@Override
	public synchronized <A> A[] toArray(A[] a) {
		return this.lista.toArray(a);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String toJson() {
		final IFilter[] array = this.lista.toArray(new IFilter[this.lista.size()]);
		
		if (array.length == 1)
			return array[0].toJson();
		
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[");

		if (array.length > 0) {
			for (IFilter iFilter : array) {
				result.append(iFilter.toJson());
				result.append(',');
			}
			result.setCharAt(result.lastIndexOf(","), ']');
		} else {
			result.append(']');
		}

		result.append(",\"data\":" + this.getDataValue() + "}");

		return result.toString();
	}
	
	private String getDataValue() {
		return "null";
	}
	
	public static String getNomeFiltro() {
		return NOMEFILTRO;
	}

}
