package model.filters;

import controller.monitor.MonitorUtente;
import controller.monitor.UserSettings;
import model.interfaces.IEquipaggio;

public class FiltroIoSegnato<T extends IEquipaggio> implements IFilter<T> {

	public static final String NOMEFILTRO = "ioSegnato";
	
	private final MonitorUtente monitor;
	
	private FiltroPersonaInEquipaggio<T> filtroPersonaInEquipaggio = null;

	public FiltroIoSegnato(MonitorUtente monitor) {
		this.monitor = monitor;
	}
	
	public FiltroIoSegnato(UserSettings impostazioni) {
		this.monitor = null;
		this.filtroPersonaInEquipaggio = new FiltroPersonaInEquipaggio<>(impostazioni.getNome());
	}
	
	@Override
	public boolean filter(T t) {
		if (this.filtroPersonaInEquipaggio == null) {
			this.filtroPersonaInEquipaggio = new FiltroPersonaInEquipaggio<>(this.monitor.getUserSettings().getNome());
		}
		return this.filtroPersonaInEquipaggio.filter(t);
	}
	
	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[],\"data\":{}}");
		return result.toString();
	}

}
