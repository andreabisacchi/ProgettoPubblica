package model.filters;

import model.interfaces.IEquipaggio;

public class FiltroCiSonoAspiranti<T extends IEquipaggio> implements IFilter<T> {

	private static final String NOMEFILTRO = "ciSonoAspiranti";

	@Override
	public boolean filter(T t) {
		return t.getEquipaggio().getPersoneEquipaggio().stream().anyMatch(persona -> persona.isAspirante());
	}

	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[],\"data\":" + this.getDataValue() + "}");
		return result.toString();
	}

	private String getDataValue() {
		return "null";
	}
	
	public static String getNomeFiltro() {
		return NOMEFILTRO;
	}
	
}
