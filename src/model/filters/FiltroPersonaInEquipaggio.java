package model.filters;

import model.interfaces.IEquipaggio;

public class FiltroPersonaInEquipaggio<T extends IEquipaggio> implements IFilter<T> {

	public static final String NOMEFILTRO = "personaInEquipaggio";
	
	private String nome;
	
	public FiltroPersonaInEquipaggio(String nome) {
		this.nome = nome.trim();
	}
	
	@Override
	public boolean filter(T t) {
		return t.getEquipaggio().getPersoneEquipaggio().stream().parallel().anyMatch(persona -> persona.getNome().equalsIgnoreCase(this.nome));
	}

	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[],\"data\":" + this.getDataValue() + "}");
		return result.toString();
	}
	
	private String getDataValue() {
		return "{\"nome\":\"" + this.nome + "\"}";
	}
	
	public static String getDataFormat() {
		return "{\"fields\":[\"nome\"],\"nome\":\"string\"}";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FiltroPersonaInEquipaggio<?> other = (FiltroPersonaInEquipaggio<?>) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	
}
