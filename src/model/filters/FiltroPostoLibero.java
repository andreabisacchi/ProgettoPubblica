package model.filters;

import model.Servizio;
import model.PersonaEquipaggio;
import model.QualificaPersonaEquipaggio;

public class FiltroPostoLibero implements IFilter<Servizio> {

	private static final String NOMEFILTRO = "postoLibero";
	
	private QualificaPersonaEquipaggio qualifica;
	
	public FiltroPostoLibero(QualificaPersonaEquipaggio qualifica) {
		this.qualifica = qualifica;
	}
	
	@Override
	public boolean filter(Servizio ambulanza) {
		for (PersonaEquipaggio persona: ambulanza.getEquipaggio()) {
			if (persona.getQualifica().equals(this.qualifica) && persona.getNome().length() > 0) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[],\"data\":" + this.getDataValue() + "}");
		return result.toString();
	}
	
	private String getDataValue() {
		return "null";
	}
	
	public static String getNomeFiltro() {
		return NOMEFILTRO;
	}
	
}
