package model.filters;

public final class FiltroNot<T> implements IFilter<T> {

	private static final String NOMEFILTRO = "not";
	private final IFilter<T> filtro;
	
	public FiltroNot(IFilter<T> filtro) {
		this.filtro = filtro;
	}
	
	@Override
	public boolean filter(T t) {
		return !this.filtro.filter(t);
	}

	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[");
		
		result.append(this.filtro.toJson());
		result.append("],\"data\":" + this.getDataValue() + "}");
		
		return result.toString();
	}
	
	private String getDataValue() {
		return "null";
	}
	
	public static String getNomeFiltro() {
		return NOMEFILTRO;
	}
	
}
