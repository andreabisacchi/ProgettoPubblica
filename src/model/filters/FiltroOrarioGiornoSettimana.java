package model.filters;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Collection;
import java.util.HashSet;

import com.google.gson.Gson;

import model.QualificaPersonaEquipaggio;
import model.QualificheGestionale;
import model.Servizio;

public class FiltroOrarioGiornoSettimana implements IFilter<Servizio> {

	public static final String NOMEFILTRO = "filtroOrarioGiornoSettimana";
	
	private final QualificheGestionale qualifica;
	
	private final HashSet<FasciaOraria> fasceOrarie;
	private final HashSet<DayOfWeek> giorniDellaSettimana;
	private final IFilter<Servizio> filtroPostoLiberoPerQualifica;

	public FiltroOrarioGiornoSettimana(Collection<FasciaOraria> fasceOrarie, Collection<DayOfWeek> giorniDellaSettimana, QualificheGestionale qualifica) {
		this.fasceOrarie = new HashSet<>(fasceOrarie);
		this.giorniDellaSettimana = new HashSet<>(giorniDellaSettimana);
		
		this.qualifica = qualifica;
		
		switch (qualifica) {
		case TERZO:
			this.filtroPostoLiberoPerQualifica = new FiltroPostoLibero(QualificaPersonaEquipaggio.TERZO);
			break;
		case SOCCORRITORE:
			this.filtroPostoLiberoPerQualifica = new FiltroPostoLibero(QualificaPersonaEquipaggio.SOCCORRITORE);
			break;
		case AUTISTA:
			this.filtroPostoLiberoPerQualifica = new FiltroPostoLibero(QualificaPersonaEquipaggio.AUTISTA);
			break;
		case AUTISTASOCCORRITORE:
			this.filtroPostoLiberoPerQualifica = new FiltroOr<>(new FiltroPostoLibero(QualificaPersonaEquipaggio.SOCCORRITORE), new FiltroPostoLibero(QualificaPersonaEquipaggio.AUTISTA));
			break;

		default:
			this.filtroPostoLiberoPerQualifica = new IFilter<Servizio>() {
				@Override
				public String toJson() {return null;}
				@Override
				public boolean filter(Servizio t) {return true;}
			};
			break;
		}
		
	}

	@Override
	public boolean filter(Servizio servizio) {
		if (this.filtroPostoLiberoPerQualifica.filter(servizio)) {
			if (this.giorniDellaSettimana.contains(servizio.getData().getDayOfWeek())) {
				for (FasciaOraria fasciaOraria : fasceOrarie) {
					if (isBetween(servizio.getOrarioInizio(), servizio.getOrarioFine(), fasciaOraria.getLocalTime())) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	private static boolean isBetween(LocalTime start, LocalTime end, LocalTime time) {
	  if (start.isAfter(end)) {
	    return !time.isBefore(start) || !time.isAfter(end);
	  } else {
	    return !time.isBefore(start) && !time.isAfter(end);
	  }
	}

	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[],\"data\":" + this.getDataValue() + "}");
		return result.toString();
	}
	
	private final static Gson gson = new Gson();
	private String getDataValue() {
		return "{\"fasciaOraria\":" + gson.toJson(this.fasceOrarie) + ",\"giorniSettimana\":" + gson.toJson(this.giorniDellaSettimana) + ",\"qualifica\":\"" + qualifica.toString() + "\"}";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fasceOrarie == null) ? 0 : fasceOrarie.hashCode());
		result = prime * result + ((giorniDellaSettimana == null) ? 0 : giorniDellaSettimana.hashCode());
		result = prime * result + ((qualifica == null) ? 0 : qualifica.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FiltroOrarioGiornoSettimana other = (FiltroOrarioGiornoSettimana) obj;
		if (fasceOrarie == null) {
			if (other.fasceOrarie != null)
				return false;
		} else if (!fasceOrarie.equals(other.fasceOrarie))
			return false;
		if (giorniDellaSettimana == null) {
			if (other.giorniDellaSettimana != null)
				return false;
		} else if (!giorniDellaSettimana.equals(other.giorniDellaSettimana))
			return false;
		if (qualifica != other.qualifica)
			return false;
		return true;
	}
	
	public QualificheGestionale getQualifica() {
		return qualifica;
	}

	public HashSet<FasciaOraria> getFasceOrarie() {
		return fasceOrarie;
	}

	public HashSet<DayOfWeek> getGiorniDellaSettimana() {
		return giorniDellaSettimana;
	}

}
