package model.filters;

public final class FiltroAnd<T> implements IFilter<T> {

	private static final String NOMEFILTRO = "and";
	
	private final IFilter<T> filtro1;
	private final IFilter<T> filtro2;
	
	public FiltroAnd(IFilter<T> filtro1, IFilter<T> filtro2) {
		this.filtro1 = filtro1;
		this.filtro2 = filtro2;
	}
	
	@Override
	public boolean filter(T t) {
		return this.filtro1.filter(t) && this.filtro2.filter(t);
	}

	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[");
		
		result.append(this.filtro1.toJson());
		result.append(',');
		result.append(this.filtro2.toJson());
		result.append("],\"data\":" + this.getDataValue() + "}");
		
		return result.toString();
	}
	
	private String getDataValue() {
		return "null";
	}
	
	public static String getNomeFiltro() {
		return NOMEFILTRO;
	}

}
