package model.filters;

import model.Manifestazione;

public class FiltroDescrizioneManifestazione implements IFilter<Manifestazione> {

	public static final String NOMEFILTRO = "descrizioneManifestazione";

	private final String descrizione;
	
	public FiltroDescrizioneManifestazione(String descrizione) {
		this.descrizione = descrizione.toLowerCase().trim();
	}

	@Override
	public boolean filter(Manifestazione manifestazione) {
		return (manifestazione.getDescrizione().toLowerCase().contains(this.descrizione) || manifestazione.getNome().toLowerCase().contains(this.descrizione));
	}

	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[],\"data\":" + this.getDataValue() + "}");
		return result.toString();
	}
	
	private String getDataValue() {
		return "{\"descrizione\":\"" + this.descrizione + "\"}";
	}

	public String getDescrizione() {
		return this.descrizione;
	}

}
