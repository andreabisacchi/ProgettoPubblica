package model.filters;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import controller.monitor.MonitorUtente;
import controller.monitor.UserSettings;
import model.interfaces.IData;
import model.interfaces.IEquipaggio;
import model.interfaces.IOrarioFine;
import model.interfaces.IOrarioInizio;

public class FiltroPromemoria<T extends IEquipaggio & IOrarioFine & IOrarioInizio & IData> implements IFilter<T> {

	private final static long ORE_PROMEMORIA = 12;
	
	public static final String NOMEFILTRO = "filtroPromemoria";
	
	private FiltroIoSegnato<T> ioSegnato;
	private FiltroNonNelPassato<T> filtroNonNelPassato = new FiltroNonNelPassato<>();
	private final MonitorUtente monitor;
	
	public FiltroPromemoria(MonitorUtente monitor) {
		this.monitor = monitor;
		this.ioSegnato = null;
	}
	
	public FiltroPromemoria(UserSettings settings) {
		this.monitor = null;
		this.ioSegnato = new FiltroIoSegnato<>(settings);
	}

	@Override
	public boolean filter(T t) {
		if (this.ioSegnato == null) {
			this.ioSegnato = new FiltroIoSegnato<>(monitor);
		}
		return this.ioSegnato.filter(t) && this.filtroNonNelPassato.filter(t) && (ChronoUnit.HOURS.between(LocalDateTime.now(), LocalDateTime.of(t.getData(), t.getOrarioInizio())) <= ORE_PROMEMORIA);
	}

	@Override
	public String toJson() {
		final StringBuilder result = new StringBuilder();
		result.append("{\"nome\":\"" + NOMEFILTRO + "\",\"children\":[],\"data\":{}}");
		return result.toString();
	}

}
