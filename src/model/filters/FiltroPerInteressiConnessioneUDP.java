package model.filters;

import java.io.Closeable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;

public abstract class FiltroPerInteressiConnessioneUDP<T> extends FiltroPerInteressi<T> implements Runnable, Closeable {

	private int port;
	private Thread runningThread = null;

	public FiltroPerInteressiConnessioneUDP() {
		this(15000);
	}

	public FiltroPerInteressiConnessioneUDP(int port) {
		super();
		this.port = port;

		if (this.runningThread != null)
			throw new IllegalArgumentException("C'e gia un'istanza attiva.");

		this.runningThread = new Thread(this, "Thread receiver filter UDP");
		this.runningThread.setDaemon(true);
		this.runningThread.start();
	}

	public void cambiaPorta(int port) {
		if (this.runningThread != null)
			throw new IllegalStateException("Il thread e attivo, non e possibile cambiare ora la porta");

		this.port = port;
	}

	@Override
	public void run() {
		try {
			DatagramSocket serverSocket = new DatagramSocket(this.port);
			while (!Thread.currentThread().isInterrupted())
			{
				byte[] receiveData = new byte[65507];
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

				serverSocket.setSoTimeout(1000 * 30);
				try {
					serverSocket.receive(receivePacket);
				} catch (SocketTimeoutException s) {}
				
				if (receivePacket != null && receivePacket.getLength() > 0 && receivePacket.getAddress() != null) {
					String ricevuto = new String(receivePacket.getData());
					this.elaboraStringaRicevuta(ricevuto);
				}
			}
			serverSocket.close();
		}
		catch (Exception e) {
			System.err.println("[FiltroPerInteressiConnessioneUDP]: Errore, il filtro non e piu in ascolto sulla porta: " + this.port);
			this.close();
		}
	}

	private void elaboraStringaRicevuta(String ricevuto) {
		String perControlli = ricevuto.trim().toUpperCase();
		if (perControlli.startsWith("AGGIUNGI:")) {
			String daAggiungere = ricevuto.trim().replaceFirst("AGGIUNGI:", "").trim();
			this.addInteresse(daAggiungere);
		} else if (perControlli.startsWith("RIMUOVI:")) {
			String daRimuovere = ricevuto.trim().replaceFirst("RIMUOVI:", "").trim();
			this.removeInteresse(daRimuovere);
		} else {
			System.err.println("[FiltroPerInteressiConnessioneUDP]: Non ho trovato corrispondenza con \"" + ricevuto + "\".");
		}
	}

	@Override
	public void close() {
		this.runningThread.interrupt();
		this.runningThread = null;
	}

}
