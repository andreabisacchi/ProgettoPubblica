package model.filters;

import model.Manifestazione;

public class FiltroManifestazioniCheMiInteressano extends FiltroPerInteressi<Manifestazione> {

	@Override
	protected String getNome(Manifestazione manifestazione) {
		return manifestazione.getNome();
	}

}
