package model.filters;

public interface IFilter<T> {
	
	public boolean filter(T t);

	public String toJson();
	
}
