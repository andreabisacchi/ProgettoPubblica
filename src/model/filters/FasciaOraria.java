package model.filters;

import java.time.LocalTime;

public enum FasciaOraria {
Mattina(LocalTime.of(10,0)), Pomeriggio(LocalTime.of(16,0)), Sera(LocalTime.of(21,0)), Notte(LocalTime.of(4,0));
	
	private LocalTime _localTime;
	
	FasciaOraria(LocalTime time) {
		this._localTime = time;
	}
	
	public LocalTime getLocalTime() {
		return this._localTime;
	}
	
}
