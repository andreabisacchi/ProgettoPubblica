package controller.monitor;

import com.google.gson.Gson;

import model.EmailList;
import model.QualificheGestionale;

public class UserSettings {
	private final EmailList emailList = new EmailList();
	private QualificheGestionale qualifica;
	private String nome;
	private String urlImmagine;
	private final String username;
	
	public UserSettings(String username) {
		this.username = username.trim();
	}
	
	public synchronized String getUsername() {
		return this.username;
	}
	
	public synchronized boolean addEmail(String email) {
		return this.emailList.add(email);
	}
	
	public synchronized boolean removeEmail(String email) {
		return this.emailList.remove(email);
	}
	
	public synchronized void clearEmails() {
		this.emailList.clear();
	}
	
	public synchronized EmailList getEmailList() {
		return this.emailList;
	}

	public synchronized QualificheGestionale getQualifica() {
		return qualifica;
	}

	public synchronized void setQualifica(QualificheGestionale qualifica) {
		this.qualifica = qualifica;
	}

	public synchronized String getNome() {
		return nome;
	}

	public synchronized void setNome(String nome) {
		this.nome = nome;
	}
	
	public synchronized String getUrlImmagine() {
		return this.urlImmagine;
	}
	
	public synchronized void setUrlImmagine(String urlImmagine) {
		this.urlImmagine = urlImmagine;
	}
	
	private static final Gson gson = new Gson();
	public synchronized String toJson() {
		return gson.toJson(this);
	}

}
