package controller.monitor;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;

public class MonitorUtenteAttivi implements Collection<MonitorUtente> {

	private final HashSet<MonitorUtente> monitorUtenteAttivi = new HashSet<>();

	private static final MonitorUtenteAttivi instance = new MonitorUtenteAttivi();
	public static MonitorUtenteAttivi getInstance() {
		return instance;
	}
	
	@Override
	public boolean add(MonitorUtente e) {
		return this.monitorUtenteAttivi.add(e);
	}

	@Override
	public boolean addAll(Collection<? extends MonitorUtente> c) {
		return this.monitorUtenteAttivi.addAll(c);
	}

	@Override
	public void clear() {
		this.monitorUtenteAttivi.clear();
	}

	@Override
	public boolean contains(Object o) {
		return this.monitorUtenteAttivi.contains(o);
	}
	
	public boolean contains(String username) {
		for (MonitorUtente monitorUtente : monitorUtenteAttivi) {
			if (monitorUtente.getUserSettings().getUsername().equals(username))
				return true;
		}
		return false;
	}
	
	public Optional<MonitorUtente> get(String username) {
		for (MonitorUtente monitorUtente : monitorUtenteAttivi) {
			if (monitorUtente.getUserSettings().getUsername().equals(username))
				return Optional.of(monitorUtente);
		}
		return Optional.empty();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return this.monitorUtenteAttivi.containsAll(c);
	}

	@Override
	public boolean isEmpty() {
		return this.monitorUtenteAttivi.isEmpty();
	}

	@Override
	public Iterator<MonitorUtente> iterator() {
		return this.monitorUtenteAttivi.iterator();
	}

	@Override
	public boolean remove(Object o) {
		return this.monitorUtenteAttivi.remove(o);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return this.monitorUtenteAttivi.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return this.monitorUtenteAttivi.retainAll(c);
	}

	@Override
	public int size() {
		return this.monitorUtenteAttivi.size();
	}

	@Override
	public Object[] toArray() {
		return this.monitorUtenteAttivi.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return this.monitorUtenteAttivi.toArray(a);
	}
	

}
