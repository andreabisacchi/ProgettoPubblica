package controller.monitor;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Optional;

import logger.Logger;
import model.Credentials;
import model.Manifestazione;
import model.Servizio;
import model.comportamenti.Comportamento;
import model.comportamenti.ComportamentoSenzaRipetizioni;
import model.comportamenti.ListaComportamenti;
import model.comportamenti.json.manifestazioni.JSONComportamentoManifestazioni;
import model.comportamenti.json.servizi.JSONComportamentoServizi;
import model.connectors.LoginConnector;
import model.connectors.ManifestazioniConnector;
import model.connectors.ServiziConnector;
import model.scanner.Scanner;
import personalizzazioni.NumeroMaxServiziManifestazioni;

public class MonitorUtente {
	
	private static final String CARTELLA_SALVATAGGI = "backups";
	private static final String PREFISSO_FILE_SALVATAGGI = "backup";
	private static final String PREFISSO_FILE_SERVIZI = CARTELLA_SALVATAGGI + File.separator + PREFISSO_FILE_SALVATAGGI + "_servizi_";
	private static final String PREFISSO_FILE_MANIFESTAZIONI = CARTELLA_SALVATAGGI + File.separator + PREFISSO_FILE_SALVATAGGI + "_manifestazioni_";
	
	private final NumeroMaxServiziManifestazioni maxID = new NumeroMaxServiziManifestazioni();
	
	private final ListaComportamenti<Servizio> listaComportamentiServizio;
	private final Scanner<Servizio> scannerServizi;
	
	private final ListaComportamenti<Manifestazione> listaComportamentiManifestazione;
	private final Scanner<Manifestazione> scannerManifestazioni;
	
	private final ThreadGroup threadGroup;
	
	private final UserSettings userSettings;
	
	private final File backupFileServizi;
	private final File backupFileManifestazioni;
	
	private boolean backupEnabled = false;
	
	public MonitorUtente(Credentials credenziali) {
		final LoginConnector loginConnector = new LoginConnector(credenziali);
		
		this.userSettings = new UserSettings(credenziali.getUsername());
		
		this.threadGroup = new ThreadGroup("Threads of " + credenziali.getUsername());
		
		this.backupFileServizi = new File(PREFISSO_FILE_SERVIZI + credenziali.getUsername());
		this.listaComportamentiServizio = new ListaComportamenti<>(this);
		this.listaComportamentiServizio.setID(0);
		this.scannerServizi = new Scanner<>(new ServiziConnector(loginConnector), this.listaComportamentiServizio);
		
		this.backupFileManifestazioni = new File(PREFISSO_FILE_MANIFESTAZIONI + credenziali.getUsername());
		this.listaComportamentiManifestazione = new ListaComportamenti<>(this);
		this.listaComportamentiManifestazione.setID(0);
		this.scannerManifestazioni = new Scanner<>(new ManifestazioniConnector(loginConnector), this.listaComportamentiManifestazione);
		
		// Carico backup
		if (this.backupFileServizi.exists()) {
			try {
				for (Comportamento<Servizio> comportamento : JSONComportamentoServizi.getComportamentiFromFile(backupFileServizi, this)) {
					this.addComportamentoServizio(comportamento);
				}
				Logger.log("Caricato il file di backup " + this.backupFileServizi.getAbsolutePath());
			} catch (Exception e) {
				Logger.error("Errore nel caricamento del backup servizi dell'utente " + credenziali.getUsername() + ". Errore: " + e.getMessage());
			}
		}
		
		if (this.backupFileManifestazioni.exists()) {
			try {
				for (Comportamento<Manifestazione> comportamento : JSONComportamentoManifestazioni.getComportamentiFromFile(backupFileManifestazioni, this)) {
					this.addComportamentoManifestazione(comportamento);
				}
				Logger.log("Caricato il file di backup " + this.backupFileManifestazioni.getAbsolutePath());
			} catch (Exception e) {
				Logger.error("Errore nel caricamento del backup manifestazioni dell'utente " + credenziali.getUsername() + ". Errore: " + e.getMessage());
			}
		}
		
		this.backupEnabled = true;
	}
	
	public final void start() {
		Logger.log("Avvio monitor utente: " + this.userSettings.getUsername());
		this.scannerManifestazioni.start(this.threadGroup);
		this.scannerServizi.start(this.threadGroup);
		MonitorUtenteAttivi.getInstance().add(this);
	}
	
	public final void stop() {
		Logger.log("Arresto monitor utente: " + this.userSettings.getUsername());
		this.scannerManifestazioni.stop();
		this.scannerServizi.stop();
		MonitorUtenteAttivi.getInstance().remove(this);
	}
	
	public final ListaComportamenti<Servizio> getListaServizi() {
		return this.listaComportamentiServizio;
	}
	
	public final ListaComportamenti<Manifestazione> getListaManifestazioni() {
		return this.listaComportamentiManifestazione;
	}
	
	public final Optional<Comportamento<Servizio>> modificaComportamentoServizio(int ID, Comportamento<Servizio> comportamento) {
		final Optional<Comportamento<Servizio>> comportamentoDaModificareOpt = this.getComportamentoServiziByID(this.listaComportamentiServizio, ID);
		if (!comportamentoDaModificareOpt.isPresent()) {
			Logger.log(this.getUserSettings().getUsername() + " ha tentato di modificato il comportamento servizi con ID " + ID + " ma non e stato trovato.");
			return Optional.empty();
		}
		
		final Comportamento<Servizio> comportamentoDaModificare = comportamentoDaModificareOpt.get();
		
		if (comportamentoDaModificare.getClass() == comportamento.getClass()) { // stessa classe --> Cambio solo il filtro
			if (comportamentoDaModificare.getFiltro().equals(comportamento.getFiltro())) // Se i filtri sono uguali --> non cambiare nulla, e solo un salva anziche un close
				return Optional.of(comportamentoDaModificare);
			
			comportamentoDaModificare.setFiltro(comportamento.getFiltro());
			if (comportamentoDaModificare instanceof ComportamentoSenzaRipetizioni) {
				((ComportamentoSenzaRipetizioni<Servizio>) comportamentoDaModificare).clearHistory();
			}
			
			Logger.log(this.getUserSettings().getUsername() + " ha modificato il comportamento servizi con ID " + comportamentoDaModificare.getID() + ". Nuovo comportamento : " + comportamento.toJson());
			this.backupServizi();
			
			return Optional.of(comportamentoDaModificare);
		} else { // diversa classe --> Imposto lo stesso ID ma cambio il comportamento
			this.listaComportamentiServizio.remove(comportamentoDaModificare);
			comportamento.setID(comportamentoDaModificare.getID());
			if (comportamento instanceof ComportamentoSenzaRipetizioni) {
				((ComportamentoSenzaRipetizioni<Servizio>) comportamento).clearHistory();
			}
			this.listaComportamentiServizio.add(comportamento);
			
			Logger.log(this.getUserSettings().getUsername() + " ha modificato il comportamento servizi con ID " + comportamentoDaModificare.getID() + ". Nuovo comportamento : " + comportamento.toJson());
			this.backupServizi();
			
			return Optional.of(comportamento);
		}
	}
	
	public final Optional<Comportamento<Manifestazione>> modificaComportamentoManifestazione(int ID, Comportamento<Manifestazione> comportamento) {
		final Optional<Comportamento<Manifestazione>> comportamentoDaModificareOpt = this.getComportamentoManifestazioniByID(this.listaComportamentiManifestazione, ID);
		if (!comportamentoDaModificareOpt.isPresent()) {
			Logger.log(this.getUserSettings().getUsername() + " ha tentato di modificato il comportamento manifestazioni con ID " + ID + " ma non e stato trovato.");
			return Optional.empty();
		}
		
		final Comportamento<Manifestazione> comportamentoDaModificare = comportamentoDaModificareOpt.get();
		
		if (comportamentoDaModificare.getClass() == comportamento.getClass()) { // stessa classe --> Cambio solo il filtro
			if (comportamentoDaModificare.getFiltro().equals(comportamento.getFiltro())) // Se i filtri sono uguali --> non cambiare nulla, e solo un salva anziche un close
				return Optional.of(comportamentoDaModificare);
			
			comportamentoDaModificare.setFiltro(comportamento.getFiltro());
			if (comportamentoDaModificare instanceof ComportamentoSenzaRipetizioni) {
				((ComportamentoSenzaRipetizioni<Manifestazione>) comportamentoDaModificare).clearHistory();
			}
			
			Logger.log(this.getUserSettings().getUsername() + " ha modificato il comportamento manifestazioni con ID " + comportamentoDaModificare.getID() + ". Nuovo comportamento : " + comportamento.toJson());
			this.backupManifestazioni();
			
			return Optional.of(comportamentoDaModificare);
		} else { // diversa classe --> Imposto lo stesso ID ma cambio il comportamento
			this.listaComportamentiServizio.remove(comportamentoDaModificare);
			comportamento.setID(comportamentoDaModificare.getID());
			if (comportamento instanceof ComportamentoSenzaRipetizioni) {
				((ComportamentoSenzaRipetizioni<Manifestazione>) comportamento).clearHistory();
			}
			this.listaComportamentiManifestazione.add(comportamento);
			
			Logger.log(this.getUserSettings().getUsername() + " ha modificato il comportamento manifestazioni con ID " + comportamentoDaModificare.getID() + ". Nuovo comportamento : " + comportamento.toJson());
			this.backupManifestazioni();
			
			return Optional.of(comportamento);
		}
	}
	
	public final boolean addComportamentoServizio(Comportamento<Servizio> comportamento) {
		final int ID = this.nextIDServizi();
		if (ID <= 0)
			return false;
		comportamento.setID(ID);
		boolean result = this.listaComportamentiServizio.add(comportamento);
		if (result && comportamento instanceof ListaComportamenti) {
			for (Comportamento<Servizio> c : (ListaComportamenti<Servizio>) comportamento) {
				final int subID = this.nextIDServizi();
				if (subID > 0)
					c.setID(subID);
				else
					result = false;
			}
		}
		
		if (result)
			Logger.log(this.getUserSettings().getUsername() + " ha creato un nuovo comportamento servizi: " + comportamento.toJson());
		else
			Logger.log(this.getUserSettings().getUsername() + " ha tentato di creare un nuovo comportamento servizi senza successo.");
		this.backupServizi();
		
		return result;
	}
	
	private int nextIDServizi() {
		for (int i = 1; i <= maxID.getNumeroMaxServizi(this.userSettings.getUsername()); i++) {
			Optional<Comportamento<Servizio>> comportamento = this.getComportamentoServiziByID(this.listaComportamentiServizio, i);
			if (!comportamento.isPresent())
				return i;
		}
		return -1;
	}

	private Optional<Comportamento<Servizio>> getComportamentoServiziByID(ListaComportamenti<Servizio> listaComportamenti, int id) {
		for (Comportamento<Servizio> comportamento : listaComportamenti) {
			if (comportamento.getID() == id) {
				return Optional.of(comportamento);
			} else if (comportamento instanceof ListaComportamenti) {
				Optional<Comportamento<Servizio>> subResult = this.getComportamentoServiziByID((ListaComportamenti<Servizio>) comportamento, id);
				if (subResult.isPresent())
					return subResult;
			}
		}
		return Optional.empty();
	}

	public final boolean removeComportamentoServizio(Comportamento<Servizio> comportamento) {
		final boolean result = this.listaComportamentiServizio.remove(comportamento);
		this.backupServizi();
		return result;
	}
	
	public final boolean removeComportamentoServizio(int ID) {
		if (ID <= 0)
			return false;
		Optional<Comportamento<Servizio>> toDelete = this.getComportamentoServiziByID(this.listaComportamentiServizio, ID);
		if (toDelete.isPresent()) {
			final boolean result = this.removeComportamentoServizio(toDelete.get());
			return result;
		}
		return false;
	}
	
	public final void clearComportamentiServizio() {
		this.listaComportamentiServizio.clear();
		this.backupServizi();
	}
	
	public final boolean addComportamentoManifestazione(Comportamento<Manifestazione> comportamento) {
		final int ID = this.nextIDManifestazioni();
		if (ID <= 0)
			return false;
		comportamento.setID(ID);
		boolean result = this.listaComportamentiManifestazione.add(comportamento);
		if (result && comportamento instanceof ListaComportamenti) {
			for (Comportamento<Manifestazione> c : (ListaComportamenti<Manifestazione>) comportamento) {
				final int subID = this.nextIDManifestazioni();
				if (subID > 0)
					c.setID(subID);
				else
					result = false;
			}
		}
		if (result)
			Logger.log(this.getUserSettings().getUsername() + " ha creato un nuovo comportamento manifestazioni: " + comportamento.toJson());
		else
			Logger.log(this.getUserSettings().getUsername() + " ha tentato di creare un nuovo comportamento manifestazioni senza successo.");
		this.backupManifestazioni();
		return result;
	}
	
	private int nextIDManifestazioni() {
		for (int i = 1; i <= maxID.getNumeroMaxManifestazioni(this.userSettings.getUsername()); i++) {
			Optional<Comportamento<Manifestazione>> comportamento = this.getComportamentoManifestazioniByID(this.listaComportamentiManifestazione, i);
			if (!comportamento.isPresent())
				return i;
		}
		return -1;
	}

	private Optional<Comportamento<Manifestazione>> getComportamentoManifestazioniByID(ListaComportamenti<Manifestazione> listaComportamenti, int id) {
		for (Comportamento<Manifestazione> comportamento : listaComportamenti) {
			if (comportamento.getID() == id) {
				return Optional.of(comportamento);
			} else if (comportamento instanceof ListaComportamenti) {
				Optional<Comportamento<Manifestazione>> subResult = this.getComportamentoManifestazioniByID((ListaComportamenti<Manifestazione>) comportamento, id);
				if (subResult.isPresent())
					return subResult;
			}
		}
		return Optional.empty();
	}
	
	public final boolean removeComportamentoManifestazione(Comportamento<Manifestazione> comportamento) {
		final boolean result = this.listaComportamentiManifestazione.remove(comportamento);
		this.backupManifestazioni();
		return result;
	}
	
	public final boolean removeComportamentoManifestazione(int ID) {
		if (ID <= 0)
			return false;
		Optional<Comportamento<Manifestazione>> toDelete = this.getComportamentoManifestazioniByID(this.listaComportamentiManifestazione, ID);
		if (toDelete.isPresent()) {
			final boolean result = this.removeComportamentoManifestazione(toDelete.get());
			return result;
		}
		return false;
	}
	
	public final void clearComportamentiManifestazione() {
		this.listaComportamentiManifestazione.clear();
		this.backupManifestazioni();
	}
	
	public final UserSettings getUserSettings() {
		return this.userSettings;
	}

	public void updateCredenziali(Credentials credenziali) {
		final LoginConnector nuovoLoginConnector = new LoginConnector(credenziali);
		this.scannerManifestazioni.setLoginConnector(nuovoLoginConnector);
		this.scannerServizi.setLoginConnector(nuovoLoginConnector);
	}
	
	private void backupServizi() {
		if (!this.backupEnabled) // Se non e attivo il backup allora non faccio nulla
			return;
		final File backupFile = this.backupFileServizi;
		final String json = JSONComportamentoServizi.jsonFromComportamento(this.listaComportamentiServizio);
		try {
			this.writeToFile(backupFile, json);
		} catch (IOException e) {
			Logger.error("Errore nel salvataggio del backup. " + e.getMessage());
		}
	}
	
	private void backupManifestazioni() {
		if (!this.backupEnabled) // Se non e attivo il backup allora non faccio nulla
			return;
		final File backupFile = this.backupFileManifestazioni;
		final String json = JSONComportamentoManifestazioni.jsonFromComportamento(this.listaComportamentiManifestazione);
		try {
			this.writeToFile(backupFile, json);
		} catch (IOException e) {
			Logger.error("Errore nel salvataggio del backup. " + e.getMessage());
		}
	}
	
	private void writeToFile(File file, String data) throws IOException {
		if (file.exists()) { // Lo cancello e lo riscrivo
			file.delete();
			file.createNewFile();
		}
		final PrintStream backupFileStream = new PrintStream(file);
		backupFileStream.print(data);
		backupFileStream.close();
		Logger.log("Scritto backup per utente " + this.getUserSettings().getUsername() + " nel file: " + file.getAbsolutePath());
	}
	
	static {
		final File folderBackup = new File(CARTELLA_SALVATAGGI);
		if (!folderBackup.isDirectory()) { // Creo la cartella se non esiste
			folderBackup.mkdirs();
			Logger.log("Creata cartella per backup files: " + folderBackup.getAbsolutePath());
		}
	}
	
}
