package controller.websocket.actions;

import java.io.IOException;
import java.util.HashMap;

import org.java_websocket.WebSocket;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import controller.monitor.MonitorUtente;
import logger.Logger;
import managers.authentication.AuthenticationManager;
import managers.authentication.NotAuthorizedException;
import model.Credentials;
import model.connectors.InformationGetterConnector;
import model.connectors.LoginConnector;
import model.connectors.NewsDaLeggereException;

public class ActionAuthentication {

	private HashMap<String, MonitorUtente> utentiCollegati = new HashMap<>();
	private static final Gson gson = new Gson();

	public boolean authenticate(WebSocket conn, String message) {
		Authentication authentication = null;
		boolean jsonError = false;
		try {
			authentication = gson.fromJson(message, Authentication.class);
			if (authentication == null || authentication.username == null || authentication.password == null) {
				jsonError = true;
			} else {
				authentication.username = authentication.username.trim();
				authentication.password = authentication.password.trim();
			}
		} catch(JsonSyntaxException e) {
			jsonError = true;
		}
		if (jsonError) {
			conn.send("ERROR JSON AUTHENTICATION");
			return false;
		}
		
		try {
			AuthenticationManager.getInstance().checkUser(authentication.username);
		} catch (NotAuthorizedException e) {
			conn.send("Errore.\n" + e.getMessage());
			return false;
		}
		
		final Credentials credenziali = new Credentials(authentication.username, authentication.password);
		final LoginConnector loginConnector = new LoginConnector(credenziali);
		
		try {loginConnector.connect();} catch (IOException e) {}
		
		boolean isConnected;
		try {
			isConnected = loginConnector.isConnected();
		} catch (NewsDaLeggereException e) {
			conn.send("News da leggere. Ti prego di aprire il gestionale e di leggere le news.");
			return false;
		}
		
		if (isConnected) {
			Logger.log("Login utente " + loginConnector.getUsername());
			final MonitorUtente monitorUtente;
			if (this.utentiCollegati.containsKey(authentication.username)) {
				monitorUtente = this.utentiCollegati.get(authentication.username);
				monitorUtente.updateCredenziali(credenziali);
			} else {
				monitorUtente = new MonitorUtente(credenziali);
				monitorUtente.start();
				this.utentiCollegati.put(authentication.username, monitorUtente);
			}
			try {
				InformationGetterConnector.insertSettingsInMonitorUtente(monitorUtente, loginConnector);
			} catch (IOException e) {
				conn.send("ERROR GETTING INFORMATIONS");
				return false;
			}
			conn.setAttachment(monitorUtente);
			conn.send("LOGIN OK");
			return true;
		} else {
			Logger.log("Errore login utente " + loginConnector.getUsername() + " IP " + conn.getRemoteSocketAddress());
			conn.send("Controlla username e password.");
			return false;
		}

	}
	
	public void closing() {
		for (MonitorUtente monitor : this.utentiCollegati.values()) {
			monitor.stop();
		}
	}

	private class Authentication {
		public String username;
		public String password;
	}
	
}