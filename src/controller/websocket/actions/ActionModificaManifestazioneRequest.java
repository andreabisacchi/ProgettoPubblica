package controller.websocket.actions;

import model.comportamenti.json.manifestazioni.JSONComportamentoManifestazioni;

class ActionModificaManifestazioneRequest extends ActionRequest {
	public int ID;
	public JSONComportamentoManifestazioni comportamento;
}
