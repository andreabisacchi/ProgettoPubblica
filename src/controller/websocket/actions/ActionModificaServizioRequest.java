package controller.websocket.actions;

import model.comportamenti.json.servizi.JSONComportamentoServizi;

class ActionModificaServizioRequest extends ActionRequest {
	public int ID;
	public JSONComportamentoServizi comportamento;
}
