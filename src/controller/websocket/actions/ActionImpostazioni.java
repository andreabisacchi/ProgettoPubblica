package controller.websocket.actions;

import org.java_websocket.WebSocket;

import com.google.gson.Gson;

import controller.monitor.MonitorUtente;
import controller.monitor.UserSettings;
import logger.Logger;

public class ActionImpostazioni extends WebsocketAction {

	private static final String ACTIONIMPOSTAZIONI = "impostazioni";

	private static final Gson gson = new Gson();

	@Override
	public boolean isApplicable(String message) {
		try {
			ActionRequest impostazioni = gson.fromJson(message, ActionRequest.class);
			return (impostazioni != null && impostazioni.action != null && impostazioni.action.equals(ACTIONIMPOSTAZIONI));
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		final MonitorUtente monitor = conn.getAttachment();
		if (monitor == null) {
			conn.send("ERROR NOT LOGGED IN");
			return;
		}
		final UserSettings settings = monitor.getUserSettings();

		final String logMessage = ((MonitorUtente) conn.getAttachment()).getUserSettings().getUsername() + " ha richiesto le impostazioni";
		Logger.log(logMessage);
		
		final String json = settings.toJson();

		conn.send(json);
	}

	@Override
	public void closing() {
		// Nothing to do
	}

}
