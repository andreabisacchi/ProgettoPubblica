package controller.websocket.actions;

import org.java_websocket.WebSocket;

import com.google.gson.Gson;

import controller.monitor.MonitorUtente;
import logger.Logger;

public class ActionEliminaManifestazione extends WebsocketAction {
	
	private static final Object ACTIONELIMINASERVIZIO = "eliminaManifestazione";

	private static final Gson gson = new Gson();

	@Override
	public boolean isApplicable(String message) {
		try {
			ActionModificaManifestazioneRequest request = gson.fromJson(message, ActionModificaManifestazioneRequest.class);
			return (request != null && request.action != null && request.action.equals(ACTIONELIMINASERVIZIO));
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		final MonitorUtente monitor = conn.getAttachment();
		if (monitor == null) {
			conn.send("ERROR NOT LOGGED IN");
			return;
		}
		final ActionModificaManifestazioneRequest request = gson.fromJson(message, ActionModificaManifestazioneRequest.class);

		final boolean result = monitor.removeComportamentoManifestazione(request.ID);

		final String logMessage = ((MonitorUtente) conn.getAttachment()).getUserSettings().getUsername() + " ha eliminato il comportamento di ID: " + request.ID;
		Logger.log(logMessage);
		
		final StringBuilder reply = new StringBuilder();

		reply.append("{\"result\":");
		reply.append(result);
		reply.append(",\"messaggio\":\"Non sono riuscito ad eliminare l'azione.\"");
		reply.append("}");

		conn.send(reply.toString());
	}

	@Override
	public void closing() {
		// TODO Auto-generated method stub
	}

}
