package controller.websocket.actions;

import org.java_websocket.WebSocket;

import com.google.gson.Gson;

import controller.monitor.MonitorUtente;
import logger.Logger;

public class ActionEliminaServizio extends WebsocketAction {

	private static final Object ACTIONELIMINASERVIZIO = "eliminaServizio";

	private static final Gson gson = new Gson();

	@Override
	public boolean isApplicable(String message) {
		try {
			ActionModificaServizioRequest request = gson.fromJson(message, ActionModificaServizioRequest.class);
			return (request != null && request.action != null && request.action.equals(ACTIONELIMINASERVIZIO));
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		final MonitorUtente monitor = conn.getAttachment();
		if (monitor == null) {
			conn.send("ERROR NOT LOGGED IN");
			return;
		}
		final ActionModificaServizioRequest request = gson.fromJson(message, ActionModificaServizioRequest.class);

		final boolean result = monitor.removeComportamentoServizio(request.ID);

		final String logMessage = ((MonitorUtente) conn.getAttachment()).getUserSettings().getUsername() + " ha eliminato il comportamentodi ID : " + request.ID;
		Logger.log(logMessage);
		
		final StringBuilder reply = new StringBuilder();

		reply.append("{\"result\":");
		reply.append(result);
		reply.append(",\"messaggio\":\"Non sono riuscito ad eliminare l'azione.\"");
		reply.append("}");

		conn.send(reply.toString());
	}

	@Override
	public void closing() {
		// TODO Auto-generated method stub
	}

}
