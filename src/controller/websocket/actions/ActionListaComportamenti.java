package controller.websocket.actions;

import org.java_websocket.WebSocket;

import com.google.gson.Gson;

import controller.monitor.MonitorUtente;
import logger.Logger;
import model.Manifestazione;
import model.Servizio;
import model.comportamenti.ListaComportamenti;

public class ActionListaComportamenti extends WebsocketAction {

	private static final String ACTIONLISTACOMPORTAMENTI = "listaAzioni"; 

	private static final Gson gson = new Gson();

	@Override
	public void onMessage(WebSocket conn, String message) {
		final MonitorUtente monitorUtente = conn.getAttachment();
		if (monitorUtente == null) {
			conn.send("ERROR NOT LOGGED IN");
			return;
		}

		final ListaComportamenti<Servizio> listaServizi = monitorUtente.getListaServizi();
		final ListaComportamenti<Manifestazione> listaManifestazioni = monitorUtente.getListaManifestazioni();

		final String logMessage = ((MonitorUtente) conn.getAttachment()).getUserSettings().getUsername() + " ha richiesto la lista comportamenti";
		Logger.log(logMessage);
		
		final StringBuilder json = new StringBuilder();

		json.append("{\"servizi\":");
		json.append(listaServizi.toJson());
		json.append(",\"manifestazioni\":");
		json.append(listaManifestazioni.toJson());
		json.append('}');

		conn.send(json.toString());
	}

	@Override
	public void closing() {
		// Nothing to do
	}

	@Override
	public boolean isApplicable(String message) {
		try {
			ActionRequest listaComportamenti = gson.fromJson(message, ActionRequest.class); // TODO gestire eccezione
			return (listaComportamenti != null && listaComportamenti.action != null && listaComportamenti.action.equals(ACTIONLISTACOMPORTAMENTI));
		} catch (Exception e) {
			return false;
		}
	}

}
