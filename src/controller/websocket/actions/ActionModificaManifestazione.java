package controller.websocket.actions;

import java.util.Optional;

import org.java_websocket.WebSocket;

import com.google.gson.Gson;

import controller.monitor.MonitorUtente;
import logger.Logger;
import model.Manifestazione;
import model.comportamenti.Comportamento;

public class ActionModificaManifestazione extends WebsocketAction {

	private static final Object ACTIONMODIFICAMANIFESTAZIONE = "modificaManifestazione";

	private static final Gson gson = new Gson();

	@Override
	public boolean isApplicable(String message) {
		try {
			ActionModificaManifestazioneRequest request = gson.fromJson(message, ActionModificaManifestazioneRequest.class);
			return (request != null && request.action != null && request.action.equals(ACTIONMODIFICAMANIFESTAZIONE));
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		final MonitorUtente monitor = conn.getAttachment();
		if (monitor == null) {
			conn.send("ERROR NOT LOGGED IN");
			return;
		}
		final ActionModificaManifestazioneRequest request = gson.fromJson(message, ActionModificaManifestazioneRequest.class);

		final Optional<Comportamento<Manifestazione>> comportamentoRequestOpt = request.comportamento.getComportamento(monitor);
		if (!comportamentoRequestOpt.isPresent()) {
			conn.send("{\"result\":false,\"messaggio\":\"Comportamento non trovato\"}");
			return;
		}
		final Comportamento<Manifestazione> comportamentoRequest = comportamentoRequestOpt.get();

		final Optional<Comportamento<Manifestazione>> nuovoComportamento;
		if (request.ID > 0) {
			nuovoComportamento = monitor.modificaComportamentoManifestazione(request.ID, comportamentoRequest);
		} else {
			if (monitor.addComportamentoManifestazione(comportamentoRequest))
				nuovoComportamento = Optional.of(comportamentoRequest);
			else
				nuovoComportamento = Optional.empty();
		}
		
		final StringBuilder logMessage = new StringBuilder();
		logMessage.append(((MonitorUtente) conn.getAttachment()).getUserSettings().getUsername());
		logMessage.append(" ha ");
		if (request.ID == -1) {
			logMessage.append("creato un nuovo comportamento.");
		} else {
			logMessage.append(" modificato il comportamento di ID=");
			logMessage.append(request.ID);
			logMessage.append('.');
		}
		logMessage.append(" Nuovo comportamento manifestazioni: ");
		logMessage.append((nuovoComportamento.isPresent() ? nuovoComportamento.get().toJson() : " <nessuno>"));
		Logger.log(logMessage.toString());

		final StringBuilder reply = new StringBuilder();

		reply.append("{\"result\":");
		reply.append(nuovoComportamento.isPresent());
		if (nuovoComportamento.isPresent()) {
			reply.append(",\"comportamento\":");
			reply.append(nuovoComportamento.get().toJson());
		} else {
			reply.append(",\"messaggio\":\"Hai raggiunto il limite massimo di azioni creabili per le manifestazioni.\"");
		}
		reply.append("}");

		conn.send(reply.toString());
	}

	@Override
	public void closing() {
		// Nothing to do
	}

}
