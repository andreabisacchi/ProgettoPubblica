package controller.websocket.actions;

import java.util.Optional;

import org.java_websocket.WebSocket;

import com.google.gson.Gson;

import controller.monitor.MonitorUtente;
import logger.Logger;
import model.Servizio;
import model.comportamenti.Comportamento;

public class ActionModificaServizio extends WebsocketAction {

	private static final Object ACTIONMODIFICASERVIZIO = "modificaServizio";

	private static final Gson gson = new Gson();

	@Override
	public boolean isApplicable(String message) {
		try {
			ActionModificaServizioRequest request = gson.fromJson(message, ActionModificaServizioRequest.class);
			return (request != null && request.action != null && request.action.equals(ACTIONMODIFICASERVIZIO));
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		final MonitorUtente monitor = conn.getAttachment();
		if (monitor == null) {
			conn.send("ERROR NOT LOGGED IN");
			return;
		}
		final ActionModificaServizioRequest request = gson.fromJson(message, ActionModificaServizioRequest.class);

		final Optional<Comportamento<Servizio>> comportamentoRequestOpt = request.comportamento.getComportamento(monitor);
		if (!comportamentoRequestOpt.isPresent()) {
			conn.send("{\"result\":false,\"messaggio\":\"Comportamento non trovato\"}");
			return;
		}
		final Comportamento<Servizio> comportamentoRequest = comportamentoRequestOpt.get();
		
		final Optional<Comportamento<Servizio>> nuovoComportamento;
		if (request.ID > 0) {
			nuovoComportamento = monitor.modificaComportamentoServizio(request.ID, comportamentoRequest);
		} else {
			if (monitor.addComportamentoServizio(comportamentoRequest))
				nuovoComportamento = Optional.of(comportamentoRequest);
			else
				nuovoComportamento = Optional.empty();
		}
		
		final StringBuilder logMessage = new StringBuilder();
		logMessage.append(((MonitorUtente) conn.getAttachment()).getUserSettings().getUsername());
		logMessage.append(" ha ");
		if (request.ID == -1) {
			logMessage.append("creato un nuovo comportamento.");
		} else {
			logMessage.append(" modificato il comportamento di ID=");
			logMessage.append(request.ID);
			logMessage.append('.');
		}
		logMessage.append(" Nuovo comportamento servizi: ");
		logMessage.append((nuovoComportamento.isPresent() ? nuovoComportamento.get().toJson() : " <nessuno>"));
		Logger.log(logMessage.toString());

		final StringBuilder reply = new StringBuilder();

		reply.append("{\"result\":");
		reply.append(nuovoComportamento.isPresent());
		if (nuovoComportamento.isPresent()) {
			reply.append(",\"comportamento\":");
			reply.append(nuovoComportamento.get().toJson());
		} else {
			reply.append(",\"messaggio\":\"Hai raggiunto il limite massimo di azioni creabili per i servizi.\"");
		}
		reply.append("}");

		conn.send(reply.toString());
	}

	@Override
	public void closing() {
		// Nothing to do
	}

}
