package controller.websocket.actions;

import org.java_websocket.WebSocket;

public abstract class WebsocketAction {
	
	public abstract boolean isApplicable(String message);
	
	public abstract void onMessage(WebSocket conn, String message);
	
	public abstract void closing();
	
}
