package controller.websocket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.java_websocket.WebSocket;
import org.java_websocket.framing.CloseFrame;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import controller.websocket.actions.ActionAuthentication;
import controller.websocket.actions.ActionEliminaServizio;
import controller.websocket.actions.ActionEliminaManifestazione;
import controller.websocket.actions.ActionImpostazioni;
import controller.websocket.actions.ActionListaComportamenti;
import controller.websocket.actions.ActionModificaManifestazione;
import controller.websocket.actions.ActionModificaServizio;
import controller.websocket.actions.WebsocketAction;
import logger.Logger;

public class WebsocketController extends WebSocketServer {

	private final List<WebsocketAction> actions = new LinkedList<>();
	private final ConcurrentHashMap<WebSocket, Boolean> websocketAuthenticated = new ConcurrentHashMap<>();
	private final ActionAuthentication actionAuthentication = new ActionAuthentication();

	private Thread cleanerThread;
	
	public WebsocketController(int port) {
		super(new InetSocketAddress(new InetSocketAddress(0).getAddress(), port));
		this.addActions();
	}
	
	public WebsocketController() {
		super();
		this.addActions();
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		Logger.log("Chiusa connessione " + conn.getRemoteSocketAddress());
		this.websocketAuthenticated.remove(conn);
	}

	@Override
	public void onError(WebSocket conn, Exception ex) {
		Logger.error("Error. " + ex.getMessage());
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		//Logger.println("Message: " + message);

		if (!isAuthenticated(conn)) {
			this.authenticate(conn, message);
		} else { // Is authenticated -> check for actions
			for (WebsocketAction action : this.actions) {
				if (action.isApplicable(message))
					action.onMessage(conn, message);
			}
		}
	}
	
	private boolean isAuthenticated(WebSocket conn) {
		return this.websocketAuthenticated.containsKey(conn) && this.websocketAuthenticated.get(conn);
	}
	
	private void authenticate(WebSocket conn, String message) {
		if (this.actionAuthentication.authenticate(conn, message)) {
			this.websocketAuthenticated.put(conn, true);
		} else {
			conn.close(CloseFrame.REFUSE, "Login error");
		}
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		Logger.log("Nuova connessione " + conn.getRemoteSocketAddress());
	}

	@Override
	public void onStart() {
		cleanerThread = new Thread(new Runnable() {
			@Override
			public void run() {
				Logger.log("Avvio Websocket controller...");
				while (!cleanerThread.isInterrupted()) {
					try {
						for (WebSocket websocket : websocketAuthenticated.keySet()) {
							if (websocket.isClosed())
								websocketAuthenticated.remove(websocket);
						}
						Thread.sleep(1000 * 60);
					} catch (InterruptedException t) {
						break;
					} catch(Throwable t) {}
				}
				Logger.log("Stopping Websocket controller...");
			}
		});
		cleanerThread.setDaemon(true);
		cleanerThread.start();
	}

	@Override
	public void stop() throws IOException, InterruptedException {
		if (cleanerThread != null)
			cleanerThread.interrupt();
		
		for (WebsocketAction action : this.actions) {
			action.closing();
		}
		this.actionAuthentication.closing();
		super.stop();
	}

	private void addActions() {
		this.actions.add(new ActionListaComportamenti());
		this.actions.add(new ActionImpostazioni());
		this.actions.add(new ActionModificaServizio());
		this.actions.add(new ActionEliminaServizio());
		this.actions.add(new ActionModificaManifestazione());
		this.actions.add(new ActionEliminaManifestazione());
	}

}
