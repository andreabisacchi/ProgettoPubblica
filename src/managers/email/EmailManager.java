package managers.email;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import logger.Logger;
import model.EmailList;
import model.MailCredentials;
import model.StampaCarina;
import model.interfaces.IEquipaggio;

public class EmailManager {

	private static final int SECONDS_TO_WAIT = 2;
	private static final int DEFAULT_THREADS = 2;

	private final javax.mail.Authenticator javaxAuthenticator;
	private final InternetAddress fromAddress;
	private final Session session;

	private final BlockingQueue<Message> sendingQueue = new LinkedBlockingDeque<>();

	private final HashSet<EmailSender> emailSenderSet = new HashSet<>();
	private final int numberOfThreads = DEFAULT_THREADS;

	private static EmailManager instance = null;
	public static EmailManager getInstance() {
		return instance;
	}
	public static void start(MailCredentials credenziali) throws AddressException {
		if (instance == null) {
			instance = new EmailManager(credenziali);
			instance._start();
		}
	}
	public static void stop() {
		if (instance != null) {
			instance._stop();
		}
		instance = null;
	}
	
	private EmailManager(MailCredentials credenziali) throws AddressException {
		this.javaxAuthenticator = new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(credenziali.getUsername(), credenziali.getPassword());
			}
		};
		this.fromAddress = new InternetAddress(credenziali.getUsername());

		final Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", 587);

		this.session = Session.getInstance(props, this.javaxAuthenticator);
	}

	private void _start() {
		for(int i = 0; i < this.numberOfThreads; i++) {
			final EmailSender emailSender = new EmailSender(this.sendingQueue);
			emailSender.start("Email sender " + (i+1));
			this.emailSenderSet.add(emailSender);
		}
	}

	private void _stop() {
		this.emailSenderSet.forEach(emailSender -> emailSender.stop());
	}

	public <T extends StampaCarina & IEquipaggio> void mandaMail(T t, EmailList emailList) throws AddressException, MessagingException {
		final String messaggio = "Avviso che esiste un nuovo evento cui hai espresso interesse:\n" + t.stampaCarina();
		final String oggetto = "Avviso nuovo evento nel gestionale Pubblica Assistenza Citta' di Bologna";
		this.mandaMail(messaggio, oggetto, emailList);
	}
	
	public void mandaMail(String messaggio, String oggetto, EmailList emailList) throws AddressException, MessagingException {
		final Message message = new MimeMessage(this.session);
		message.setFrom(this.fromAddress);
		message.setRecipients(Message.RecipientType.TO,	InternetAddress.parse(emailListToString(emailList)));
		message.setSubject(oggetto);
		message.setText(messaggio);

		this.sendingQueue.add(message);
	}
	
	private String emailListToString(EmailList emailList) {
		final StringBuilder list = new StringBuilder();
		emailList.getEmailList().forEach(email -> {list.append(email); list.append(',');});

		String result = list.toString();
		result = result.substring(0, result.length()-1);

		return result;
	}

	private class EmailSender implements Runnable {
		private AtomicBoolean isRunning = new AtomicBoolean(true);
		private final BlockingQueue<Message> sendingQueue;
		private Thread thread = null;

		public EmailSender(BlockingQueue<Message> sendingQueue) {
			this.sendingQueue = sendingQueue;
		}

		public void stop() {
			this.isRunning.set(false);
			this.thread.interrupt();
		}
		
		public void start(String name) {
			this.thread = new Thread(this, name);
			this.thread.setDaemon(true);
			this.thread.start();
		}

		@Override
		public void run() {
			Logger.log(Thread.currentThread().getName() + " is starting...");
			try {
				while (this.isRunning.get()) {
					final Message message = this.sendingQueue.take();
					try {
						Transport.send(message);
						Logger.log("Inviata email a " + Arrays.toString(message.getRecipients(RecipientType.TO)));
						Thread.sleep(1000 * SECONDS_TO_WAIT);
					} catch (MessagingException e) {
						Logger.error("Error on sending email.");
					}
				}
			} catch (InterruptedException e) {
				// Exit
			}
			Logger.log(Thread.currentThread().getName() + " is terminating...");
		}
	}

}
