package managers.authentication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import logger.Logger;

public class AuthenticationManagerLimitedUsers extends AuthenticationManager {

	private static final int MINUTI_AGGIORNAMENTO_FILE = 5;

	private Set<String> usernameAutorizzati;
	private Gson gson = null;

	public AuthenticationManagerLimitedUsers(String[] autorizzati) {
		this(Arrays.asList(autorizzati));
	}

	public AuthenticationManagerLimitedUsers(String fileAutorizzati) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		this(new String[0]);
		
		this.gson = new Gson();
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				while (true) {
					try {
						final HashSet<String> nuoviUsernameAutorizzati = new HashSet<>(Arrays.asList(gson.fromJson(new JsonReader(new FileReader(fileAutorizzati)), String[].class)));
						synchronized (usernameAutorizzati) {
							if (!nuoviUsernameAutorizzati.equals(usernameAutorizzati)) {
								usernameAutorizzati = nuoviUsernameAutorizzati;
								Logger.log("Nuovi utenti autorizzati: " + usernameAutorizzati);
							}
						}
					} catch(FileNotFoundException e) {
						File file = new File(fileAutorizzati);
						if (!file.exists()) {
							try {
								file.createNewFile();
								PrintStream ps = new PrintStream(file);
								ps.print("[]");
								ps.close();
							} catch (IOException e1) {
								Logger.error("Errore nella creazione del file autorizzati. " + e1.getMessage());
							}
						}
						continue;
					}
					catch (Exception e) {
						Logger.error(e.getMessage());
					}
					try {
						Thread.sleep(1000 * 60 * MINUTI_AGGIORNAMENTO_FILE);
					} catch (Exception e) {}
				}
			}
		}, "Thread aggiornamento autorizzati");
		t.setDaemon(true);
		t.start();
	}

	public AuthenticationManagerLimitedUsers(Collection<String> autorizzati) {
		this.usernameAutorizzati = new HashSet<>(autorizzati);
		Logger.log("Utenti autorizzati: " + usernameAutorizzati);
	}

	@Override
	public void checkUser(String username) throws NotAuthorizedException {
		username = username.trim();
		synchronized (usernameAutorizzati) {
			if (!usernameAutorizzati.contains(username)) {
				Logger.log("Utente non autorizzato: " + username);
				throw new NotAuthorizedException("Utente " + username + " non autorizzato.");
			}
		}
	}

}
