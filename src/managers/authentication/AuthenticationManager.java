package managers.authentication;

public abstract class AuthenticationManager {

	private static AuthenticationManager instance = new DefaultAuthenticationManager();
	
	public static final AuthenticationManager getInstance() {
		return instance;
	}
	
	private static final IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
	public static final void setAuthenticatorManager(AuthenticationManager authenticationManager) {
		if (authenticationManager == null)
			throw illegalArgumentException;
		instance = authenticationManager;
	}
	
	public abstract void checkUser(String username) throws NotAuthorizedException;

}
