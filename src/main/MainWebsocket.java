package main;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.concurrent.Semaphore;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.xml.bind.DatatypeConverter;

import org.java_websocket.server.DefaultSSLWebSocketServerFactory;

import controller.websocket.WebsocketController;
import logger.Logger;
import managers.authentication.AuthenticationManager;
import managers.authentication.AuthenticationManagerLimitedUsers;
import managers.email.EmailManager;
import model.MailCredentials;

public class MainWebsocket {

	public static void main(String[] args) throws Exception {
		checkArgs(args);
		
		if (args.length >= 2) {
			Logger.setLogFile(args[1]);
		}
		
		if (args.length >= 3) {
			Logger.setErrorFile(args[2]);
		}
		
		Logger.log("PID : " + getPid());

		EmailManager.start(new MailCredentials("credenzialiEmail.json"));

		final String autorizzatiFileName = "autorizzati.json";
		AuthenticationManager.setAuthenticatorManager(new AuthenticationManagerLimitedUsers(autorizzatiFileName));

		final WebsocketController websocket = new WebsocketController(4443);

		final SSLContext context = getContext(args[0]);

		if( context != null ) {
			websocket.setWebSocketFactory( new DefaultSSLWebSocketServerFactory( context ) );
		}
		websocket.setConnectionLostTimeout(300);

		websocket.start();

		final Semaphore semafotoUscita = new Semaphore(0);

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					websocket.stop();
					EmailManager.stop();
					Thread.sleep(1000);
					semafotoUscita.release();
				} catch (Exception e) {
					Logger.error("Error on exiting websocket.");
				}
			}
		});

		semafotoUscita.acquire();

		Logger.log("Exiting...");
		System.exit(0);
	}

	private static void checkArgs(String[] args) {
		if (args.length < 1) {
			System.err.println("Il primo argomento DEVE essere la cartella dei certificati");
			printUsage();
			System.exit(1);
		}
	}
	
	private static void printUsage() {
		System.out.println("Usage:");
		System.out.println("java -jar ProgettoPubblica.jar PERCORSO_CARTELLA_CERTIFICATI [LOG_FILE] [ERROR_FILE]");
	}

	private static SSLContext getContext(final String pathname) {
		final SSLContext context;
		final String password = "P4sSw0rD!*#@";
		//final String pathname = "PATH_TO_FOLDER";
		try {
			context = SSLContext.getInstance( "TLS" );

			final byte[] certBytes = parseDERFromPEM( getBytes( new File( pathname + File.separator + "cert.pem" ) ), "-----BEGIN CERTIFICATE-----", "-----END CERTIFICATE-----" );
			final byte[] keyBytes = parseDERFromPEM( getBytes( new File( pathname + File.separator + "privkey.pem" ) ), "-----BEGIN PRIVATE KEY-----", "-----END PRIVATE KEY-----" );

			final X509Certificate cert = generateCertificateFromDER( certBytes );
			final RSAPrivateKey key = generatePrivateKeyFromDER( keyBytes );

			final KeyStore keystore = KeyStore.getInstance( "JKS" );
			keystore.load( null );
			keystore.setCertificateEntry( "cert-alias", cert );
			keystore.setKeyEntry( "key-alias", key, password.toCharArray(), new Certificate[]{ cert } );

			final KeyManagerFactory kmf = KeyManagerFactory.getInstance( "SunX509" );
			kmf.init( keystore, password.toCharArray() );

			final KeyManager[] km = kmf.getKeyManagers();

			context.init( km, null, null );
		} catch ( Exception e ) {
			return null;
		}
		return context;
	}

	private static byte[] parseDERFromPEM( byte[] pem, String beginDelimiter, String endDelimiter ) {
		String data = new String( pem );
		String[] tokens = data.split( beginDelimiter );
		tokens = tokens[1].split( endDelimiter );
		return DatatypeConverter.parseBase64Binary( tokens[0] );
	}

	private static RSAPrivateKey generatePrivateKeyFromDER( byte[] keyBytes ) throws InvalidKeySpecException, NoSuchAlgorithmException {
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec( keyBytes );

		KeyFactory factory = KeyFactory.getInstance( "RSA" );

		return ( RSAPrivateKey ) factory.generatePrivate( spec );
	}

	private static X509Certificate generateCertificateFromDER( byte[] certBytes ) throws CertificateException {
		CertificateFactory factory = CertificateFactory.getInstance( "X.509" );

		return ( X509Certificate ) factory.generateCertificate( new ByteArrayInputStream( certBytes ) );
	}

	private static byte[] getBytes( File file ) {
		byte[] bytesArray = new byte[( int ) file.length()];

		FileInputStream fis = null;
		try {
			fis = new FileInputStream( file );
			fis.read( bytesArray ); //read file into bytes[]
			fis.close();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
		return bytesArray;
	}

	private static int getPid() {
		try {
			byte[] bo = new byte[256];
			InputStream is = new FileInputStream("/proc/self/stat");
			is.read(bo);
			for (int i = 0; i < bo.length; i++) {
				if ((bo[i] < '0') || (bo[i] > '9')) {
					is.close();
					return Integer.parseInt(new String(bo, 0, i));
				}
			}
			is.close();
		} catch (Exception e) {}
		return -1;
	}

}
