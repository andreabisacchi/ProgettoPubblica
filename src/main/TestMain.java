package main;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import model.Credentials;
import model.Servizio;
import model.connectors.LoginConnector;
import model.connectors.ServiziConnector;

public class TestMain {

	public static void main(String[] args) throws Exception {
		Credentials credenziali = new Credentials("credenziali.json");
		
		ServiziConnector serviziConnector = new ServiziConnector(new LoginConnector(credenziali));
		
		LocalDate now = LocalDate.now();
		LocalDate startWeek = now;
		LocalDate endWeek = now;
		
		for (int i = 0; endWeek.getMonthValue() >= 3; i++) {
			startWeek = now.minusWeeks(i).with(TemporalAdjusters.previous(DayOfWeek.MONDAY));
			endWeek = now.minusWeeks(i).with(TemporalAdjusters.next(DayOfWeek.SUNDAY));
			String start = startWeek.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			String end = endWeek.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			System.out.println("Settimana dal " + start + " al " + end + ":");
			if (i==0)
				stampaDaVadoData(serviziConnector, "now");
			else
				stampaDaVadoData(serviziConnector, "before");
			System.out.println("");
		}
	
		
		//turni.addAll(serviziConnector.getEquipaggi("before"));
		
		//turni.forEach(s -> System.out.println(s.stampaCarina()));
		
	}
	
	private static void stampaDaVadoData(ServiziConnector serviziConnector, String vado_data) throws Exception {
		Set<Servizio> servizi = serviziConnector.getEquipaggi(vado_data);
		System.out.println(((Servizio)servizi.toArray()[0]).getData().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		Map<String, Long> counted = servizi.stream().parallel().map(s -> s.getEquipaggio()).flatMap(e -> e.getPersoneEquipaggio().stream()).map(p -> p.getNome()).filter(nome -> nome.length() > 0 && !nome.contains("Ip bs")).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		Map<String, Long> sortedCounted = new LinkedHashMap<>();
        counted.entrySet().stream().parallel()
        	.filter(e -> e.getValue() >= 4)
            .sorted(Map.Entry.<String, Long> comparingByValue().reversed())
            .forEachOrdered(e -> sortedCounted.put(e.getKey(), e.getValue()));
        System.out.println(sortedCounted);
        System.out.println("Persone con >= 4 servizi: " + sortedCounted.size() + " totale persone: " + counted.size() + ".\t\t%=" + (sortedCounted.size()*100.0/counted.size()));
	}

}
