package main;

import java.io.IOException;

import controller.monitor.MonitorUtente;
import model.Credentials;
import model.EmailList;
import model.MailCredentials;
import model.Manifestazione;
import model.Servizio;
import model.comportamenti.ComportamentoStampa;
import model.comportamenti.ListaComportamenti;
import model.filters.FilterList;
import model.filters.FiltroManifestazioniCheMiInteressano;
import model.filters.FiltroPersonaInEquipaggio;

public class Main {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException, InterruptedException {
		
		Credentials credenziali = new Credentials("credenziali.json");
		
		MonitorUtente monitorUtente = new MonitorUtente(credenziali);
		
		MailCredentials credenzialiEmail = new MailCredentials("credenzialiEmail.json");
		
		EmailList emailList = new EmailList("mailList.json");
		
		FilterList<Manifestazione> filtriManifestazione = new FilterList<>();
		
		FiltroManifestazioniCheMiInteressano filtroManifestazioniEmail = new FiltroManifestazioniCheMiInteressano();
		filtroManifestazioniEmail.addInteresse("Virtus");
		filtroManifestazioniEmail.addInteresse("Befana");
		filtroManifestazioniEmail.addInteresse("Cremonini");
		filtroManifestazioniEmail.addInteresse("Pasqua");
		filtroManifestazioniEmail.addInteresse("Carnevale");
		//filtroManifestazioniEmail.addInteresse("Bologna");
		filtroManifestazioniEmail.addInteresse("50 SPECIAL");
		filtroManifestazioniEmail.addInteresse("Green Day");
		
		filtriManifestazione.add(filtroManifestazioniEmail);
		
		
		ListaComportamenti<Manifestazione> listaComportamentiManifestazioni = new ListaComportamenti<>(monitorUtente);
		ListaComportamenti<Servizio> listaComportamentiServizi = new ListaComportamenti<>(monitorUtente);
		
		//listaComportamentiManifestazioni.add(new ComportamentoMandaMail<>(monitorUtente, 12, emailList, filtriManifestazione));
		listaComportamentiManifestazioni.add(new ComportamentoStampa<>(monitorUtente, 12, filtriManifestazione, true));
		
		
		FilterList<Servizio> filtriServiziStampa = new FilterList<>();
		FiltroPersonaInEquipaggio<Servizio> filtroIoSegnato = new FiltroPersonaInEquipaggio<>("Bisacchi Andrea");
		filtriServiziStampa.add(filtroIoSegnato);
		listaComportamentiServizi.add(new ComportamentoStampa<>(monitorUtente, 4, filtriServiziStampa, true));
		
		FilterList<Servizio> filtriAmbulanzaMail = new FilterList<>();
		FiltroPersonaInEquipaggio<Servizio> filtroPersonaSegnata = new FiltroPersonaInEquipaggio<>("Bisacchi Andrea");
		filtriAmbulanzaMail.add(filtroPersonaSegnata);
		
		//listaComportamentiServizi.add(new ComportamentoMandaMail<>(monitorUtente, 4, emailList, filtriAmbulanzaMail));
		
		monitorUtente.addComportamentoServizio(listaComportamentiServizi);
		monitorUtente.addComportamentoManifestazione(listaComportamentiManifestazioni);
		monitorUtente.start();
		
		System.out.println("Scanner partiti.\nAttendo 1h alla chiusura.");
		
		Thread.sleep(1000 * 60 * 60);
		
		System.out.println("Scanner interroti.");
		
		monitorUtente.stop();
		
	}

}
