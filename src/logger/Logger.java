package logger;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {

	private static PrintStream logFile = System.out;
	private static PrintStream errorFile = System.err;
	
	public static void log(String line) {
		final String date = getTimestamp();
		logFile.println("[" + date + "] @(" + Thread.currentThread().getName() + ") " + line);
	}

	public static void error(String line) {
		final String date = getTimestamp();
		errorFile.println(date + " " + line);
	}
	
	public static void setLogFile(String filename) throws FileNotFoundException {
		logFile = new PrintStream(filename);
	}
	
	public static void setErrorFile(String filename) throws FileNotFoundException {
		errorFile = new PrintStream(filename);
	}
	
	private static String getTimestamp() {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		return LocalDateTime.now().format(formatter);
	}
	
}
