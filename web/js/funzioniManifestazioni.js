function clearAzioniManifestazioni() {
    if ($("#azioniManifestazioni .card-body").length > 0)
        $("#azioniManifestazioni .card-body").remove();
}

function showManifestazione(manifestazione) {
    let div = document.createElement("div");
    div.className = "card-body text-white op-0-8-5 " + classDaNomeFiltroManifestazioni(manifestazione.filtro);
    let messaggio = messaggioDaFiltroManifestazione(manifestazione.filtro);
    div.innerHTML =    "<h5 class='card-title'>Ricevi <span>" + manifestazione.nome + "</span></h5>";
    div.innerHTML +=    "<p class='card-text'> Quando <span>" + messaggio + "</span></p>";
    div.innerHTML +=    "<button type='button' class='btn btn-warning' data-toggle='modal' data-target='#modalModificaManifestazioni' onclick='modificaAzioneManifestazione(" + manifestazione.ID + ");'><span class='glyphicon glyphicon-pencil'></span> Modifica</button>";  
    div.innerHTML +=    "<button type='button' class='btn btn-danger' onclick='eliminaManifestazione(" + manifestazione.ID + ");'><span class='glyphicon glyphicon-remove'></span> Elimina</button>";  
    document.getElementById("azioniManifestazioni").appendChild(div);
}

function classDaNomeFiltroManifestazioni(filtro) {
    if (filtro == null) {
        return "";
    } else {
        switch (filtro.nome) {
            case "luogoManifestazione":
                return "bg-success";

            case "descrizioneManifestazione":
                return "bg-primary";

        }
    }
}

function messaggioDaFiltroManifestazione(filtro) {
    if (filtro == null) {
        return "sempre";
    } else {
        switch (filtro.nome) {
            case "luogoManifestazione":
                return "il luogo è " + filtro.data.luogo;

            case "descrizioneManifestazione":
                return "la descrizione contiene " + filtro.data.descrizione;
        }
    }
}

function pulisciModalManifestazioni() {
    $("#nomeAzioneManifestazioni").val("email");
    $("#idAzioneManifestazione").val("");

    $("#luogoManifestazione").val("");
    $("#descrizioneManifestazione").val("");

    selectCardManifestazioni("");
}

function modificaAzioneManifestazione(id) {
    pulisciModalManifestazioni();
    let manifestazione = recuperaAzioneDaIDManifestazioni(id);
    if (manifestazione === undefined) {
        document.getElementById("modificaTitleManifestazioni").innerHTML = "Inserisci";
        return; // è una nuova manifestazione
    }
    document.getElementById("modificaTitleManifestazioni").innerHTML = "Modifica";
    
    $("#nomeAzioneManifestazioni").val(manifestazione.nome);
    $("#idAzioneManifestazione").val(id);
    
    if (manifestazione.filtro != undefined) {
        if (manifestazione.filtro.nome === "luogoManifestazione") {
                selectCardManifestazioni("card-luogo-evento");
                $("#luogoManifestazione").val(manifestazione.filtro.data.luogo);
        } else if (manifestazione.filtro.nome === "descrizioneManifestazione") {
            selectCardManifestazioni("card-descrizione-evento");
            $("#descrizioneManifestazione").val(manifestazione.filtro.data.descrizione);
        }
    }
}

function recuperaAzioneDaIDManifestazioni(id) {
    if (listaAzioni.manifestazioni.azioni != undefined) { // more then 1 action:
        for (azione of listaAzioni.manifestazioni.azioni) {
            if (azione.ID == id) {
                return azione;
            }
        }
    } else {
        if (listaAzioni.manifestazioni.ID == id)
            return listaAzioni.manifestazioni;
    }
    return undefined;
}

function eliminaManifestazione(id) {
    BootstrapDialog.confirm({
        title: 'Confermi?',
        message: "Confermi l'eliminazione dell'azione?",
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        btnCancelLabel: 'Annulla', // <-- Default value is 'Cancel',
        btnOKLabel: 'Cancella', // <-- Default value is 'OK',
        btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
        callback: function(result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if(result) {
                pulisciModalManifestazioni();
                let request = new Object();
                request.ID = id;
                request.action = "eliminaManifestazione";

                websocket.onmessage = function(evt) { waitForDeleteOK(evt) };
                doSend(JSON.stringify(request));
            }else {
                // Ha cliccato di non confermare
            }
        }
    });
}

function salvaManifestazione() {
    if (!checkFormManifestazione()) {
        BootstrapDialog.alert({
                title: 'Inserisci tutti i campi',
                message: 'Alcuni campi obbligatori non sono stati inseriti.\nInserisci tutti i campi',
                type: BootstrapDialog.TYPE_WARNING,
                closable: true
            });
        return;
    }

    let request = new Object();
    request.ID = parseInt($("#idAzioneManifestazione").val());

    if (isNaN(request.ID))
        request.ID = -1;

    request.action = "modificaManifestazione";
    let manifestazione = new Object();
    manifestazione.nome = $("#nomeAzioneManifestazioni").val();
    
    manifestazione.filtro = new Object();
    manifestazione.filtro.data = new Object();

    switch ($("#selectedCardManifestazione").val()) {
        case "card-luogo-evento":
            manifestazione.filtro.nome = "luogoManifestazione";

            manifestazione.filtro.data.luogo = $("#luogoManifestazione").val();
            break;
        
        case "card-descrizione-evento":
            manifestazione.filtro.nome = "descrizioneManifestazione";

            manifestazione.filtro.data.descrizione = $("#descrizioneManifestazione").val();
            break;
        
        default:
            return;
    }
    
    
    request.comportamento = manifestazione;

    websocket.onmessage = function(evt) { waitForSaveOK(evt) };
    doSend(JSON.stringify(request));
    
    chiudiModalManifestazioni();
}

function selectCardManifestazioni(id) {
    $("#selectedCardManifestazione").val(id);
    cambiaCardManifestazioni(id);
}

function cambiaCardManifestazioni(id) {
    if (id === "card-luogo-evento") {
        $("#card-luogo-evento").addClass("card-selected");
        $("#card-luogo-evento").removeClass("card-not-selected");
    } else  {
        $("#card-luogo-evento").removeClass("card-selected");
        $("#card-luogo-evento").addClass("card-not-selected");
    }
    if (id === "card-descrizione-evento") {
        $("#card-descrizione-evento").addClass("card-selected");
        $("#card-descrizione-evento").removeClass("card-not-selected");
    } else  {
        $("#card-descrizione-evento").removeClass("card-selected");
        $("#card-descrizione-evento").addClass("card-not-selected");
    }
}

function checkFormManifestazione() {
    if (document.getElementById("selectedCardManifestazione").value === "")
        return false;
    let form = document.getElementById('form-modificaManifestazioni');
    for(var i=0; i < form.elements.length; i++){
      if(form.elements[i].value === '' && form.elements[i].hasAttribute('required')){
        return false;
      }
    }
    return true;
}

function chiudiModalManifestazioni() {
    if ($("#modalModificaManifestazioni").is(":visible"))
        $("#modalModificaManifestazioni").modal("toggle");
}