var wsUri = "wss:/andre-bisa.ddns.net:4443";
var websocket;

var listaAzioni;
var impostazioni;

function init()
{
    websocket = null;
}

function checkDati()
{
    showWaitLogin();

    if (websocket != null)
        return;
    websocket = new WebSocket(wsUri);
    websocket.onopen = (evt) => onOpen(evt);
    websocket.onclose = (evt) => onClose(evt);
    //websocket.onmessage = (evt) => onMessage(evt);
    websocket.onerror = (evt) => onError(evt);
}

function onOpen(evt)
{
    console.log("CONNECTED");
    let login = new Object();
    login.username = document.getElementById("username").value;
    login.password = document.getElementById("password").value;

    websocket.onmessage = (evt) => waitForLoginOK(evt);
    doSend(JSON.stringify(login));
}

function onClose(evt)
{
    console.log("DISCONNECTED");
    disconnect();
}

function onMessage(evt)
{
    console.log("Messaggio: " + evt.data);
}

function waitForLoginOK(evt) {
    if (evt.data == "LOGIN OK") {
        websocket.onmessage = (evt) => waitForImpostazioni(evt) ;

        let action = new Object();
        action.action = "impostazioni";
        doSend(JSON.stringify(action));
    } else {
        loginError();
        if (evt != null)
            document.getElementById("login-error-message").innerHTML = evt.data;
    }
}

function waitForImpostazioni(evt) {
    if (evt.data != null) {
        websocket.onmessage = (evt) => waitForListaAzioni(evt) ;
        impostazioni = JSON.parse(evt.data);

        let action = new Object();
        action.action = "listaAzioni";
        doSend(JSON.stringify(action));

        if (impostazioni != null) {
            showImpostazioni();
            loginSuccessful();
        }
    } else {
        disconnect();
    }
}

function waitForListaAzioni(evt) {
    if (evt.data != null) {
        websocket.onmessage = (evt) => onMessage(evt);
        listaAzioni = JSON.parse(evt.data);
        if (listaAzioni != undefined) {
            showAzioni();
        }
    } else {
        disconnect();
    }
}

function onError(evt)
{
    console.log("Error: " + evt.data);
    loginError();
}

function doSend(message)
{
    websocket.send(message);
}

function clearAzioni() {
    clearAzioniServizi();
    clearAzioniManifestazioni();
}

function showAzioni() {
    clearAzioni();
    if (listaAzioni.servizi.azioni != undefined) // more then 1 action:
        listaAzioni.servizi.azioni.forEach(azione => showAzioneServizio(azione));
    else
        showAzioneServizio(listaAzioni.servizi);
    
    if (listaAzioni.manifestazioni.azioni != undefined) // more then 1 action:
        listaAzioni.manifestazioni.azioni.forEach(manifestazione => showManifestazione(manifestazione));
    else
        showManifestazione(listaAzioni.manifestazioni);
}

function classDaNomeFiltro(filtro) {
    if (filtro == null) {
        return "";
    } else {
        switch (filtro.nome) {
            case "ioSegnato":
            case "luogoManifestazione":
                return "bg-primary";

            case "filtroOrarioGiornoSettimana":
            case "descrizioneManifestazione":
                return "bg-success";

        }
    }
}

function messaggioGiorniSettimana(giorniSettimana) {
    if (giorniSettimana.length === 7) {
        return "tutti i giorni";
    } else if (giorniSettimana.length === 1) {
        return "il " + trasformaGiornoIngleseInItaliano(giorniSettimana[0]);
    } else {
        return giorniSettimana.length + " giorni selezionati";
    }
    return result.substring(0, result.length - 1);
}

function trasformaGiornoIngleseInItaliano(giorno) {
    switch (giorno) {
            case "MONDAY":
                return "lun";
            case "TUESDAY":
                return "mar";
            case "WEDNESDAY":
                return "mer";
            case "THURSDAY":
                return "gio";
            case "FRIDAY":
                return "ven";
            case "SATURDAY":
                return "sab";
            case "SUNDAY":
                return "dom";
        }
}

function showImpostazioni() {
    document.getElementById("utenteImmagine").src = impostazioni.urlImmagine;
    document.getElementById("utenteNome").innerHTML = impostazioni.nome;
    document.getElementById("utenteQualifica").innerHTML = impostazioni.qualifica;
    if (impostazioni.emailList != undefined && impostazioni.emailList.length >= 1)
        document.getElementById("utenteEmail").innerHTML = impostazioni.emailList[0];
}

function waitForDeleteOK(evt) {
    if (evt.data != null) {
        let risposta = JSON.parse(evt.data);
        if (risposta.result) {
            BootstrapDialog.alert({
                title: 'Eliminato con successo',
                message: 'Eliminato con successo',
                type: BootstrapDialog.TYPE_SUCCESS,
                closable: true
            });
        } else {
            BootstrapDialog.alert({
                title: "Errore nell'eliminazione",
                message: "Errore nell'eliminazione.\n" + risposta.messaggio,
                type: BootstrapDialog.TYPE_DANGER,
                closable: true
            });
        }
        websocket.onmessage = function(evt) { waitForListaAzioni(evt) };

        let action = new Object();
        action.action = "listaAzioni";
        doSend(JSON.stringify(action));
    } else {
        disconnect();
    }
}

function waitForSaveOK(evt) {
    if (evt.data != null) {
        let risposta = JSON.parse(evt.data);
        if (risposta.result) {
            BootstrapDialog.alert({
                title: 'Salvato con successo',
                message: 'Salvato con successo',
                type: BootstrapDialog.TYPE_SUCCESS,
                closable: true
            });
        } else {
            BootstrapDialog.alert({
                title: "Errore nel salvataggio",
                message: "Errore nel salvataggio.\n" + risposta.messaggio,
                type: BootstrapDialog.TYPE_DANGER,
                closable: true
            });
        }
        websocket.onmessage = function(evt) { waitForListaAzioni(evt) };

        let action = new Object();
        action.action = "listaAzioni";
        doSend(JSON.stringify(action));
    } else {
        disconnect();
    }
}

function showWaitLogin() {
    $("#attesaLogin").show();
    $("#login-button").attr("disabled", true);
    $("#username").attr("disabled", true);
    $("#password").attr("disabled", true);
}

function hideWaitLogin() {
    $("#attesaLogin").hide();
    $("#login-button").attr("disabled", false);
    $("#username").attr("disabled", false);
    $("#password").attr("disabled", false);
}

function disconnect()
{
    //document.getElementById("login").className = "animated bounceInDown";
    //$("#login-container").removeClass("animated bounceOutUp");

    if (!$("#login-wrapper").hasClass("shake")) {
        //$("#login-wrapper").addClass("animated bounceInDown");
        $("#login-container").show();
        $("#login-wrapper").addClass("bounceInDown");
        setTimeout(function(){$("#login-wrapper").removeClass("bounceInDown");}, 1500);
    }

    hideWaitLogin();
    document.getElementById("main-container").style.display = "none";
    
    chiudiModalServizi();
    chiudiModalManifestazioni();

    if (websocket == null)
      return;
    websocket.close();
    websocket = null;
}

function loginError() {
    //$("#login-wrapper").removeClass("animated");
    //$("#login-wrapper").removeClass("bounceInDown");
    setTimeout(function(){$("#login-wrapper").removeClass("shake");}, 1500);
    $("#login-wrapper").addClass("shake");
    //$("#login-wrapper").addClass("animated");

    disconnect();

    $("#login-error").show();
    document.getElementById("main-container").style.display = "none";
}

function loginSuccessful() { // called when login is done correctly
    document.getElementById("main-container").style.display = "flex";
    
    $("#login-container").addClass("bounceOutUp");
    setTimeout(function(){
        $("#login-container").removeClass("bounceOutUp");
        $("#login-container").hide();
    }, 500);

    // Pre-select tab
    Array.from(document.getElementsByClassName("defaultTab")).forEach(div => div.click());

    $("#login-error").hide();

    hideWaitLogin();
}

window.addEventListener("load", init, false);
