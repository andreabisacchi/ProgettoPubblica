function clearAzioniServizi() {
    if ($("#azioniServizi .card-body").length > 0)
        $("#azioniServizi .card-body").remove();
}

function showAzioneServizio(azione) {
    let div = document.createElement("div");
    div.className = "card-body text-white op-0-8-5 " + classDaNomeFiltroServizi(azione.filtro);
    let messaggio = messaggioDaFiltroServizio(azione.filtro);
    div.innerHTML =    "<h5 class='card-title'>Ricevi <span>" + azione.nome + "</span></h5>";
    div.innerHTML +=    "<p class='card-text'> <span>" + messaggio + "</span></p>";
    div.innerHTML +=    "<button type='button' class='btn btn-warning' data-toggle='modal' data-target='#modalModificaServizi' onclick='modificaAzioneServizio(" + azione.ID + ");'><span class='glyphicon glyphicon-pencil'></span> Modifica</button>";  
    div.innerHTML +=    "<button type='button' class='btn btn-danger' onclick='eliminaServizio(" + azione.ID + ");'><span class='glyphicon glyphicon-remove'></span> Elimina</button>";  
    document.getElementById("azioniServizi").appendChild(div);
}

function classDaNomeFiltroServizi(filtro) {
    if (filtro == null) {
        return "";
    } else {
        switch (filtro.nome) {
            case "ioSegnato":
            case "filtroPromemoria":
                return "bg-primary";

            case "filtroOrarioGiornoSettimana":
                return "bg-success";

        }
    }
}

function messaggioDaFiltroServizio(filtro) {
    if (filtro == null) {
        return "sempre";
    } else {
        switch (filtro.nome) {
            case "ioSegnato":
                return "Quando sono segnato";

            case "filtroPromemoria":
                return "12 ore prima del turno";

            case "filtroOrarioGiornoSettimana":
                return "Quando c'è un posto libero da " + filtro.data.qualifica.toLocaleLowerCase() + " " +
                 messaggioGiorniSettimana(filtro.data.giorniSettimana);
        }
    }
}

function pulisciModalServizi() {
    $("#nomeAzioneServizio").val("email");
    $("#idAzioneServizio").val("");

    selectCardServizi("");
    
    if (impostazioni !== undefined && impostazioni.qualifica !== undefined)
        $("#postoLiberoQualifica").val(impostazioni.qualifica);

    $("#fasciaOraria").multiselect('deselectAll', false);
    $('#fasciaOraria').multiselect('updateButtonText');

    $("#giorniSettimana").multiselect('deselectAll', false);
    $('#giorniSettimana').multiselect('updateButtonText');

}

function modificaAzioneServizio(id) {
    pulisciModalServizi();
    let azione = recuperaAzioneDaIDServizi(id);
    if (azione === undefined) {
        document.getElementById("modificaTitleServizi").innerHTML = "Inserisci";
        return; // è una nuova azione
    }
    document.getElementById("modificaTitleServizi").innerHTML = "Modifica";
    
    $("#nomeAzioneServizio").val(azione.nome);
    $("#idAzioneServizio").val(id);
    
    if (azione.filtro != undefined) {
        if (azione.filtro.nome === "ioSegnato") {
            selectCardServizi("card-io-segnato");
        } else if (azione.filtro.nome === "filtroPromemoria") {
            selectCardServizi("card-promemoria");
        } else if (azione.filtro.nome === "filtroOrarioGiornoSettimana") {
            selectCardServizi("card-giorno-orario");
            $("#postoLiberoQualifica").val(azione.filtro.data.qualifica);

            $("#fasciaOraria").multiselect('select', azione.filtro.data.fasciaOraria);
            $('#fasciaOraria').multiselect('updateButtonText');

            $("#giorniSettimana").multiselect('select', azione.filtro.data.giorniSettimana);
            $('#giorniSettimana').multiselect('updateButtonText');
        }
    }
}

function recuperaAzioneDaIDServizi(id) {
    if (listaAzioni.servizi.azioni != undefined) { // more then 1 action:
        for (azione of listaAzioni.servizi.azioni) {
            if (azione.ID == id) {
                return azione;
            }
        }
    } else {
        if (listaAzioni.servizi.ID == id)
            return listaAzioni.servizi;
    }
    return undefined;
}

function eliminaServizio(id) {
    BootstrapDialog.confirm({
        title: 'Confermi?',
        message: "Confermi l'eliminazione dell'azione?",
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        btnCancelLabel: 'Annulla', // <-- Default value is 'Cancel',
        btnOKLabel: 'Cancella', // <-- Default value is 'OK',
        btnOKClass: 'btn-danger', // <-- If you didn't specify it, dialog type will be used,
        callback: function(result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if(result) {
                pulisciModalServizi();
                let request = new Object();
                request.ID = id;
                request.action = "eliminaServizio";

                websocket.onmessage = function(evt) { waitForDeleteOK(evt) };
                doSend(JSON.stringify(request));
            }else {
                // Ha cliccato di non confermare
            }
        }
    });
}

function salvaServizio() {
    if (!checkFormServizio()) {
        BootstrapDialog.alert({
                title: 'Inserisci tutti i campi',
                message: 'Alcuni campi obbligatori non sono stati inseriti.\nInserisci tutti i campi',
                type: BootstrapDialog.TYPE_WARNING,
                closable: true
            });
        return;
    }

    let request = new Object();
    request.ID = parseInt($("#idAzioneServizio").val());

    if (isNaN(request.ID))
        request.ID = -1;

    request.action = "modificaServizio";
    let azione = new Object();
    azione.nome = $("#nomeAzioneServizio").val();
    
    azione.filtro = new Object();
    azione.filtro.data = new Object();

    switch ($("#selectedCardServizio").val()) {
        case "card-io-segnato":
            azione.filtro.nome = "ioSegnato";
            break;

        case "card-promemoria":
            azione.filtro.nome = "filtroPromemoria";
            break;
        
        case "card-giorno-orario":
            azione.filtro.nome = "filtroOrarioGiornoSettimana";

            let fasciaOrariaSelezionati = $('#fasciaOraria').children(':selected');
            let numeroSelezionatiFasciaOraria = fasciaOrariaSelezionati.length;
            azione.filtro.data.fasciaOraria = new Array();
            for(let i = 0; i < numeroSelezionatiFasciaOraria; i++) {
                azione.filtro.data.fasciaOraria[i] = fasciaOrariaSelezionati[i].value;
            }

            let giorniSettimanaSelezionati = $('#giorniSettimana').children(':selected');
            let numeroSelezionatiGiorniSettimana = giorniSettimanaSelezionati.length;
            azione.filtro.data.giorniSettimana = new Array();
            for(let i = 0; i < numeroSelezionatiGiorniSettimana; i++) { 
                azione.filtro.data.giorniSettimana[i] = giorniSettimanaSelezionati[i].value;
            }

            azione.filtro.data.qualifica = $("#postoLiberoQualifica").val();

            break;
        
        default:
            return;
    }
    
    
    request.comportamento = azione;

    websocket.onmessage = function(evt) { waitForSaveOK(evt) };
    doSend(JSON.stringify(request));
    
    chiudiModalServizi();
}

function selectCardServizi(id) {
    $("#selectedCardServizio").val(id);
    cambiaCardServizi(id);
}

function cambiaCardServizi(id) {
    if (id === "card-io-segnato") {
        $("#card-io-segnato").addClass("card-selected");
        $("#card-io-segnato").removeClass("card-not-selected");
    } else {
        $("#card-io-segnato").removeClass("card-selected");
        $("#card-io-segnato").addClass("card-not-selected");
    }

    if (id === "card-promemoria") {
        $("#card-promemoria").addClass("card-selected");
        $("#card-promemoria").removeClass("card-not-selected");
    } else {
        $("#card-promemoria").removeClass("card-selected");
        $("#card-promemoria").addClass("card-not-selected");
    }

    if (id === "card-giorno-orario") {
        $("#card-giorno-orario").addClass("card-selected");
        $("#card-giorno-orario").removeClass("card-not-selected");
        $("#fasciaOraria").attr("required", true);
        $("#postoLiberoQualifica").attr("required", true);
        $("#giorniSettimana").attr("required", true);
    } else  {
        $("#card-giorno-orario").removeClass("card-selected");
        $("#card-giorno-orario").addClass("card-not-selected");
        $("#fasciaOraria").attr("required", false);
        $("#postoLiberoQualifica").attr("required", false);
        $("#giorniSettimana").attr("required", false);
    }
}

function checkFormServizio() {
    if (document.getElementById("selectedCardServizio").value === "")
        return false;
    let form = document.getElementById('form-modificaServizi');
    for(var i=0; i < form.elements.length; i++){
      if(form.elements[i].value === '' && form.elements[i].hasAttribute('required')){
        return false;
      }
    }
    return true;
}

function chiudiModalServizi() {
    if ($("#modalModificaServizi").is(":visible"))
        $("#modalModificaServizi").modal("toggle");
}